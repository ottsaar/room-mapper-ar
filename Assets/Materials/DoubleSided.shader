﻿    Shader "Transparent/DoubleSided" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    }
     
    SubShader {
        Tags {"RenderType"="Transparent" "Queue"="Transparent"}
        // Render into depth buffer only
        Pass {
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask RGB
            Cull Off
            Material {
                Diffuse [_Color]
            }
            Lighting Off
            SetTexture [_MainTex] {
                Combine texture * primary DOUBLE, texture * primary
            }
        }
     
    }
    }
