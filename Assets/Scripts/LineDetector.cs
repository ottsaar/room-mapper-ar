﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDetector:MonoBehaviour{
	public static int threshold = 14;//don't know at the moment
	//can be higher, if bad performance with 1
	private static int  degreeStep = 1;
	//can be higher, if performance with 1 pixel at a time if bad performance.
	public static int pixelStep = 7;
    private static float cvWidth;
    private static float cvHeight;
    private static float scHeight;
    private static float scWidth;
	public static Dictionary<Vector2,int> DetectLines(byte[] outputImage, int width, int height,int orgWidth,int orgHeight)
	{
        //Assinging values
        cvWidth = width;
        cvHeight = height;
        scWidth = orgWidth;
        scHeight = orgHeight;

		//dictionary, where line is key and value is the times some kind of edge was on it.
		Dictionary<Vector2,int> allLinePoints = new Dictionary<Vector2, int> ();
		//getting the pixel of the image -> outputImage[(j * width) + i];
		int y = 0;
		for (int x = 0; x < width; x += pixelStep) {
			if (x >= width || x + pixelStep >= width) {
				y += pixelStep;
				x = 0;
			}
			if (y >= height)
				break;
			//if equals 0xFF then is an edge and should start controlling
			if (outputImage [(y * width) + x] == 0xFF) {
				//finding all the possible lines from the current point by degreeStep
				for (int o = -90; o < 90; o += degreeStep) {
                    //we know that doors are in most cases upright, so we can remove most of the middle degrees
                    if (o == -80 || o == 10)
                    {
                        if (o < 0)
                            o = -10;
                        else
                            o = 80;
                    }
					float rad =	Mathf.Deg2Rad * o;
					float r = 0;
                    r = Mathf.Abs(x * Mathf.Cos(rad) + y * Mathf.Sin(rad));
					if (r != 0) {
						Vector2 key = new Vector2 (r, rad);
						int countCurrently = 0;
						try {
							countCurrently = allLinePoints [key];
						} catch (KeyNotFoundException e) {
							countCurrently = 0;
						}
						allLinePoints [key] = countCurrently + 1;
					}
				}

            }
		}
		return allLinePoints;
	}

	public static List<Vector2[]> FilteringOutLines(Dictionary<Vector2,int> allLinePoints)
    {
		List<Vector2> keysList = new List<Vector2> (allLinePoints.Keys);
		//all the lines that are smaller then threshold doesn't have enough edges on it.
		foreach (Vector2 key in keysList) {	
            int value = allLinePoints[key];
			if (value < threshold) {
				allLinePoints.Remove (key);
			}
		}
		//parallel line pairs.
		List<Vector2[]> paraLines = new List<Vector2[]> ();
		keysList = new List<Vector2> (allLinePoints.Keys);
        float difDegree = Mathf.Deg2Rad * 2; // 3 degree
        for (int i = 0; i < keysList.Count; i++) {
			for (int j = i + 1; j < keysList.Count; j++) {
                //calculating if the two lines are almost the same (+- 2 degrees) and that the rhos are different
				if ((keysList[i].y - difDegree <= keysList[j].y && keysList[j].y <= keysList[i].y + difDegree) && keysList [i].x != keysList [j].x)
                {
					if(keysList[i].x > keysList[j].x)
						paraLines.Add (new Vector2[]{ keysList [i], keysList [j] });
					else
						paraLines.Add (new Vector2[]{ keysList [j], keysList [i] });
				}
					
			}
		}

		List<Vector2[]> fourLines = new List<Vector2[]> ();

        for (int i = 0; i < paraLines.Count; i++)
        {
			for (int j = i+1; j < paraLines.Count; j++)
            {
                Vector2[] result = null;
				if (paraLines [i] [0].y > paraLines [j] [0].y && paraLines[i][0].x != paraLines[j][0].x)
                {
					result = helperFunction (paraLines [i], paraLines [j]);
				}
                else if(paraLines[j][0].y < paraLines[i][0].y && paraLines[i][0].x != paraLines[j][0].x)
                {
                   result = helperFunction(paraLines [j], paraLines [i]);
				}
                if (result != null)
                    fourLines.Add(result);
			}
		}
		return fourLines;
	}

	/// <summary>
	/// Finds the biggest rectangle in the list of rectangles
	/// </summary>
	/// <returns>The rectangle.</returns>
	/// <param name="list">list of rectangles</param>
	public static List<Vector2[]> fiveBiggestRectangles(List<Vector2[]> list){
        List<Vector2[]> fiveRectangles = new List<Vector2[]>();
        List<float> sizes = new List<float>();
		foreach (Vector2[] rectangle in list) {
            float xLength = Vector2.Distance(rectangle[0], rectangle[1]);
            float yLength = Vector2.Distance(rectangle[0], rectangle[2]);
            float size = xLength * yLength;
            if (xLength > 200 && yLength > 200) {
                if(fiveRectangles.Contains(rectangle))
                    continue;
                if(fiveRectangles.Count < 5)
                {
                    fiveRectangles.Add(rectangle);
                    sizes.Add(size);
                    continue;
                }
				for(int i = 0; i < 5; i++)
                {
                    if(size > sizes[i])
                    {
                        sizes[i] = size;
                        fiveRectangles[i] = rectangle;
                    }
                }
			}
		}
		return fiveRectangles;
	}

    /// <summary>
    /// calculates and returns the rectangle with corners
    /// </summary>
    /// <param name="a">first pair of paralleel lines</param>
    /// <param name="b">second pair of paralleel lines</param>
    /// <returns></returns>
    static Vector2[] helperFunction(Vector2[] a,Vector2[] b){
		float difference = Mathf.Deg2Rad * 90; //90 degrees
        float twoDegree = Mathf.Deg2Rad *2;
		
        //if a - b || b - a +- 2 is 90 then is rectangle
		if (((a[0].y - b[0].y) - twoDegree <= difference && difference <= (a[0].y - b[0].y) + twoDegree) || ((b[0].y - a[0].y) - twoDegree <= difference && difference <= (b[0].y - a[0].y) + twoDegree))
        {
			return new Vector2[] 
            {
                computeIntersection(a[0],b[0]),
                computeIntersection(a[1],b[0]),
                computeIntersection(a[0],b[1]),
                computeIntersection(a[1],b[1])
            };

        }
        return null;
	}
    /// <summary>
    /// finds the intersecting point of two lines
    /// https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.solve.html
    /// I do what np.linalg.solve - does automatically.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    static Vector2  computeIntersection(Vector2 a,Vector2 b)
    {
        Vector2 aThetas = new Vector2(Mathf.Cos(a[1]), Mathf.Sin(a[1]));
        Vector2 bThetas = new Vector2(Mathf.Cos(b[1]), Mathf.Sin(b[1]));
        //y of the corner(intersecting point)
        float y = (a[0] - b[0] * aThetas[0] / bThetas[0]) / (aThetas[1] - aThetas[0] * bThetas[1] / bThetas[0]);
        //x of the corner(intersecting point)
        float x = (a[0] - b[0] * aThetas[1] / bThetas[1]) / (aThetas[0] - aThetas[1] * bThetas[0] / bThetas[1]);
        Debug.Log("nii:"+a[0]+","+aThetas+";"+b[0]+","+bThetas);
        Debug.Log("tere:"+x+";"+y);
        //this should be like upscaled version, since original reso is bigger
        ///and computervision sees in 640x480, so there is a difference
        float screenX = x / cvWidth * scWidth ;
        //from up to down is y, but in unity it is in "-" (ie 0;-480), so need to transfer to "+" range. The values were smaller then they were, so a bit of conversion of values needed to be done(1+cvHeight/scHeight)
        float screenY = scHeight + (y *(1+cvHeight / scHeight))/ cvHeight * scHeight + scHeight/cvHeight*100;//+scHeight/cvHeight*100 compensates for conversion that was made (values got dragged to the left from the original position)
        if(Screen.orientation == ScreenOrientation.Portrait)
        {
            screenX = scWidth + (y *(1+cvHeight/scWidth)) / cvHeight * scWidth + scWidth/cvHeight*100; // same as before to y
            screenY = scHeight - x  / cvWidth * scHeight;
        }
        Debug.Log("enne punkt:" + screenX + ";" + screenY);
        return new Vector2(screenX,screenY);        
	}
}
