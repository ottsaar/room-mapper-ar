﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour {
    private AppController appController;
    void Awake()
    {
        appController = AppController.Instance;
    }

    public void OnClick()
    {
        appController.ToProjectMenu();
    }
}
