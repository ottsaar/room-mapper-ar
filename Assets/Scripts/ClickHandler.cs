﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    //What will it become after holding btn
    public enum ButtonType {NewWall,OneCornerChange,SameCornerChange };
    public ButtonType currentType;
	//Different sprites for different buttons
	public Sprite newWallSprite;
	public Sprite newHeightSprite;
    public Sprite sameCornerSprite;
	public Sprite sameWallSprite;
  

	/// <summary>
	/// The other buttons, that is used in the placement
	/// </summary>
	public GameObject otherButton1;
    public GameObject otherButton2;
    /// <summary>
    /// tutorial handler
    /// </summary>
    public TutorialHandler tutorial;
    //Timer used for hold-long timekeeping
    private float timer;
	private bool addedButtons=false;
	private bool clicked=false;
	//Time, how long does btn have to be held down for extra features
	private float holdTime = 1f;
	//Plane Creator object.
	private PlaneCreator creator;

	void Update()
	{
		if (clicked && !addedButtons) 
		{
			//if held down for a long time, activates otherbutton or disable other button
			if (timer + holdTime <= Time.time && creator.PlacedVertices.Count >= 4) 
			{
				if (otherButton1.activeInHierarchy)
                {
					otherButton1.SetActive (false);
                    otherButton2.SetActive (false);
					GetComponent<Image> ().sprite = sameWallSprite;
				} 
				else 
				{
                    tutorial.ClickedOn("b4");
                    otherButton1.SetActive (true);
                    otherButton2.SetActive (true);
                    if (currentType == ButtonType.NewWall)
                    {
                        GetComponent<Image>().sprite = newWallSprite;
                        otherButton1.GetComponent<Image>().sprite = newHeightSprite;
                        otherButton2.GetComponent<Image>().sprite = sameCornerSprite;
                    }
                    else if (currentType == ButtonType.OneCornerChange)
					{
						GetComponent<Image> ().sprite = newHeightSprite;
						otherButton1.GetComponent<Image> ().sprite = newWallSprite;
                        otherButton2.GetComponent<Image>().sprite = sameCornerSprite;
                    }
                    else if(currentType == ButtonType.SameCornerChange)
                    {
                        GetComponent<Image>().sprite = sameCornerSprite;
                        otherButton1.GetComponent<Image>().sprite = newWallSprite;
                        otherButton2.GetComponent<Image>().sprite = newHeightSprite;
                    }
				}
				addedButtons = true;
			}
		}
	}

	void Start()
	{
		creator = FindObjectOfType<PlaneCreator> ();
	}
	/// <summary>
	/// Method called when pointer is down, marking down time..
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerDown(PointerEventData eventData)
	{
		timer = Time.time;
		clicked = true;
	}
	/// <summary>
	/// Method called when pointer is up
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerUp(PointerEventData eventData)
	{

		//if not held down for a long time, then proceed to act as a click
		if(!addedButtons)
		{
			//if only one button active in scene, then act as a placer
			if (!otherButton1.activeInHierarchy) 
			{
				creator.PlaceVertex ();
				if (creator.PlacedVertices.Count >= 4) {
                    tutorial.ClickedOn("b3");
					GetComponent<Image> ().sprite = sameWallSprite;    
                }
                else if(creator.PlacedVertices.Count >= 2)
                {  
                    if(currentType == ButtonType.SameCornerChange)
                    {
                        Vector3 first = creator.PlacedVertices[0];
                        creator.PlacedVertices[1] = new Vector3(first.x, creator.PlacedVertices[1].y, first.z);
                    }
                    tutorial.ClickedOn("b2");
                    DottedLineRenderer.permission = true;
                    DottedLineRenderer.lastPosition = creator.PlacedVertices[creator.PlacedVertices.Count - 2];
                }
                else
                {
                    DottedLineRenderer.permission = false;
                }
            } 
			//if new wallbtn, then reset planeCreator(basically)
			else if (currentType == ButtonType.NewWall) 
			{
				creator.EmptyVertices ();
				creator.zeroHeight ();
				creator.PlaceVertex ();
				otherButton1.SetActive (false);
                otherButton2.SetActive(false);
				GetComponent<Image> ().sprite = newWallSprite;
			} 
			//button places a new vertex and is ready for new height.
			else  if(currentType == ButtonType.OneCornerChange)
			{
				creator.zeroHeight ();
				creator.PlaceVertex ();
                otherButton1.SetActive(false);
                otherButton2.SetActive(false);
                GetComponent<Image> ().sprite = newHeightSprite;
                DottedLineRenderer.lastPosition = creator.PlacedVertices[creator.PlacedVertices.Count-1];
			}
            else if (currentType == ButtonType.SameCornerChange)
            {
                if (creator.PlacedVertices.Count >= 4)
                {
                    Vector3 first = creator.PlacedVertices[creator.PlacedVertices.Count - 2];
                    creator.EmptyVertices();
                    creator.PlacedVertices.Add(first);
                }
                creator.zeroHeight();
                ObjToRay.DoPut();
                otherButton1.SetActive(false);
                otherButton2.SetActive(false);
                GetComponent<Image>().sprite = sameCornerSprite;
                DottedLineRenderer.lastPosition = creator.PlacedVertices[creator.PlacedVertices.Count - 1];
            }
		}
		clicked = false;
		addedButtons = false;
	}
}
