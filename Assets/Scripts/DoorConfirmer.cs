﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorConfirmer : MonoBehaviour {
    public DoorWindowObject refDoor;
    public void IsDoor()
    {
        GameObject room = FindObjectOfType<RoomHandler>().SelectedRoom;
        room.GetComponent<Room>().AddDoor(refDoor);
        refDoor.transform.parent = room.transform;
        refDoor.gameObject.GetComponentInChildren<Renderer>().material.mainTexture = refDoor.normalTex;
        Destroy(gameObject);
    }
    public void NoDoor()
    {
        Destroy(refDoor.gameObject);
        Destroy(gameObject);
    }
}
