﻿/*
 * I found the code from here  : http://wiki.unity3d.com/index.php/ExportOBJ
 * edited and modified it to work with my application and in runtime.
 * -Ott Saar
 * 
 */

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Text;

public class ObjExporterScript
{
    private static int StartIndex = 0;

    public static void Start()
    {
        StartIndex = 0;
    }
    public static void End()
    {
        StartIndex = 0;
    }


    public static string MeshToString(MeshFilter mf, Transform t)
    {
        Vector3 s = t.localScale;
        Vector3 p = t.localPosition;
        Quaternion r = t.localRotation;


        int numVertices = 0;
        Mesh m = mf.mesh;
        if (!m)
        {
            return "####Error####";
        }
        Material[] mats = mf.GetComponent<Renderer>().sharedMaterials;

        StringBuilder sb = new StringBuilder();

        foreach (Vector3 vv in m.vertices)
        {
            Vector3 v = t.TransformPoint(vv);
            numVertices++;
            sb.Append(string.Format("v {0} {1} {2}\n", v.x, v.y, -v.z));
        }
        sb.Append("\n");
        foreach (Vector3 nn in m.normals)
        {
            Vector3 v = r * nn;
            sb.Append(string.Format("vn {0} {1} {2}\n", -v.x, -v.y, v.z));
        }
        sb.Append("\n");
        for (int material = 0; material < m.subMeshCount; material++)
        {

            int[] triangles = m.GetTriangles(material);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
                    triangles[i] + 1 + StartIndex, triangles[i + 1] + 1 + StartIndex, triangles[i + 2] + 1 + StartIndex));
            }
        }

        StartIndex += numVertices;
        return sb.ToString();
    }
}

public class ObjExporter
{
    public static GameObject selectedObject;
 
    public static bool ExportObject()
    {
        bool result = false;
        try
        {
            result = DoExport(true);
        }
        catch(AndroidJavaException e)
        {
            result = false;
        }
        return result;
    }

    static bool DoExport(bool makeSubmeshes)
    {
        if (selectedObject == null)
        {
            Debug.Log("Didn't Export Any Meshes; Nothing was selected!");
            return false;
        }
        string meshName = selectedObject.name;
        string fileName = meshName+".obj";

        ObjExporterScript.Start();

        StringBuilder meshString = new StringBuilder();

        meshString.Append("#" +meshName + ".obj"
                            + "\n#" + System.DateTime.Now.ToLongDateString()
                            + "\n#" + System.DateTime.Now.ToLongTimeString()
                            + "\n#-------"
                            + "\n\n");

        Transform t = selectedObject.transform;

        Vector3 originalPosition = t.position;
        t.position = Vector3.zero;

        if (!makeSubmeshes)
        {
            meshString.Append("g ").Append(t.name).Append("\n");
        }
        meshString.Append(processTransform(t, makeSubmeshes));
        string dataPath = "storage/emulater/0/ExportObjects";
        bool worked = true;
        if (!Directory.Exists(dataPath))
        {
            try
            {
                Directory.CreateDirectory(dataPath);
                worked = true;
            }
            catch(System.UnauthorizedAccessException)
            {
                worked = false;
            }
        }
        if(!worked)
            dataPath = System.IO.Path.Combine(Application.persistentDataPath, "ExportedObjects");
        if (!Directory.Exists(dataPath))
            Directory.CreateDirectory(dataPath);
        WriteToFile(meshString.ToString(), dataPath +"/"+ fileName);

        t.position = originalPosition;

        ObjExporterScript.End();
        return true;
    }

    static string processTransform(Transform t, bool makeSubmeshes)
    {
        StringBuilder meshString = new StringBuilder();

        //That accidentaly any helper objects wouldn't be written into file.
        if(t.name == "Cube")
            return meshString.ToString();
        else if(t.name.Contains("Sphere"))
            return meshString.ToString();
        else if(t.name.Contains("Arrow"))
            return meshString.ToString();

        meshString.Append("#" + t.name
                        + "\n#-------"
                        + "\n");

        if (makeSubmeshes)
        {
            meshString.Append("g ").Append(t.name).Append("\n");
        }

        MeshFilter mf = t.GetComponent<MeshFilter>();
        if (mf)
        {
            meshString.Append(ObjExporterScript.MeshToString(mf, t));
        }

        for (int i = 0; i < t.childCount; i++)
        {
            meshString.Append(processTransform(t.GetChild(i), makeSubmeshes));
        }

        return meshString.ToString();
    }

    static void WriteToFile(string s, string filename)
    {
        using (StreamWriter sw = new StreamWriter(filename))
        {
            sw.Write(s);
        }
    }
}