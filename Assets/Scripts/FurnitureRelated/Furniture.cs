﻿using GoogleARCore.Examples.BasicCalc;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;

public class Furniture : EditableObject {
    //used for saving data
    public FurnitureData furnitureData = new FurnitureData();

	//Info that the furniture Game Object needs to know about itself
	public GameObject Model;
	public Material OutlinedMat;
	private Material defaultMat;

    void Start()
    {
        defaultMat = Model.GetComponent<Renderer>().material;
    }

    /// <summary>
    /// Selected this instance
    /// </summary>
    public override void Selected()
    {
        base.Selected();
        Color color = defaultMat.color;
        Model.GetComponent<Renderer>().material = OutlinedMat;
        Model.GetComponent<Renderer>().material.color = color;
    }

    /// <summary>
    /// Deselected this instance.
    /// </summary>
    public override void Deselected()
    {
        Color color = Model.GetComponent<Renderer>().material.color;
        Model.GetComponent<Renderer>().material = defaultMat;
        defaultMat.color = color;
        gameObject.GetComponent<BoxCollider>().enabled = true;
        ResetToggleButtons();
        base.Deselected();
    }

    /// <summary>
    /// Stores data to furnitureData
    /// </summary>
    public void StoreData()
    {
        string name = gameObject.name.Split('(')[0];
        Vector3 pos = transform.localPosition;
        Vector3 rot = transform.localEulerAngles;
        Vector3 size = transform.localScale;
        Color color = Model.GetComponent<Renderer>().material.color;

        furnitureData.name = name;

        furnitureData.posX = pos.x;
        furnitureData.posY = pos.y;
        furnitureData.posZ = pos.z;

        furnitureData.rotX = rot.x;
        furnitureData.rotY = rot.y;
        furnitureData.rotZ = rot.z;

        furnitureData.sizeX = size.x;
        furnitureData.sizeY = size.y;
        furnitureData.sizeZ = size.z;

        furnitureData.colorR = color.r;
        furnitureData.colorG = color.g;
        furnitureData.colorB = color.b;
    }

    /// <summary>
    /// Loads data from furnitureData
    /// </summary>
    public void LoadData()
    {
        Vector3 pos = new Vector3(furnitureData.posX, furnitureData.posY, furnitureData.posZ);
        Vector3 rot = new Vector3(furnitureData.rotX, furnitureData.rotY, furnitureData.rotZ);
        Vector3 size = new Vector3(furnitureData.sizeX, furnitureData.sizeY, furnitureData.sizeZ);
        Color color = new Color(furnitureData.colorR, furnitureData.colorG, furnitureData.colorB);

        print("furniture given: "+pos);
        transform.localPosition = pos;
        print("furniture local: "+transform.localPosition);
        transform.localEulerAngles = rot;
        transform.localScale = size;
        Model.GetComponent<Renderer>().material.color = color;
    }
}
public class FurnitureData
{
    [XmlAttribute("Name")]
    public string name;
    
    [XmlElement("PosX")]
    public float posX;
    [XmlElement("PosY")]
    public float posY;
    [XmlElement("PosZ")]
    public float posZ;

    [XmlElement("RotX")]
    public float rotX;
    [XmlElement("RotY")]
    public float rotY;
    [XmlElement("RotZ")]
    public float rotZ;

    [XmlElement("SizeX")]
    public float sizeX;
    [XmlElement("SizeY")]
    public float sizeY;
    [XmlElement("SizeZ")]
    public float sizeZ;

    [XmlElement("ColorR")]
    public float colorR;
    [XmlElement("ColorG")]
    public float colorG;
    [XmlElement("ColorB")]
    public float colorB;
}
