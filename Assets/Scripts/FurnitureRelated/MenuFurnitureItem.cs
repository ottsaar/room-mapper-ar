﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuFurnitureItem : MonoBehaviour,IDragHandler,IBeginDragHandler,IEndDragHandler{
    public string name;
    public GameObject prefab;
    private Vector3 startingPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        EditorCameraController.allowMovement = false;
        startingPosition = transform.position;
        eventData.selectedObject = gameObject;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        EditorCameraController.allowMovement = true;
        transform.position = startingPosition;
    }
}
