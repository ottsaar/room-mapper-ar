﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovementController : MonoBehaviour
{
    public bool lookAtCamera = false;
    public float PrecisionDegress = 0;
    private bool touched;
    private bool Precision = false;
    private float rotated = 0;
    private RoomHandler controller;
    private EditableObject.ObjectState currentState = EditableObject.ObjectState.Idle;
	
	// Update is called once per frame
	void Update ()
    {
        //ends update if currentstate is idle or coloring
        if (currentState == EditableObject.ObjectState.Idle ||  currentState == EditableObject.ObjectState.Coloring)
        {
            return;
        }
            
        Touch touch;

        if (lookAtCamera)
        {
            transform.LookAt(controller.SelectedCamera.transform);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            lookAtCamera = false;
        }

        //if no touch input then end update.
        if (Input.touchCount < 1)
		{
			return;
		}
        touch = Input.GetTouch(0);
        //on different states calls correct method
        if (currentState == EditableObject.ObjectState.Moving)
        {
            Move(touch);
        }
        else if (currentState == EditableObject.ObjectState.Resizing)
        {
            Resize(touch);
        }
        else if (currentState == EditableObject.ObjectState.Rotation)
        {
            Rotate(touch);
        }
  
    }

    /// <summary>
    /// Moving method
    /// </summary>
    /// <param name="touch"></param>
    void Move(Touch touch)
    {
        if(touch.phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = controller.SelectedCamera.ScreenPointToRay(touch.position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    GetComponent<EditableObject>().EnableCollision(false);
                }
            }
        }
        //if moving finger on screen, then move selected object
        if (touch.phase == TouchPhase.Moved)
        {
            RaycastHit hit;
            Ray ray = controller.SelectedCamera.ScreenPointToRay(touch.position);
            
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 moveTo = hit.point;
                transform.position = new Vector3(moveTo.x, controller.VirtualPlane.transform.position.y, moveTo.z);
            }
        }

        if(touch.phase == TouchPhase.Ended)
        {
            GetComponent<EditableObject>().EnableCollision(true);
        }
    }

    /// <summary>
    /// Resizing method
    /// </summary>
    /// <param name="touch"></param>
    void Resize(Touch touch)
    {
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (Mathf.Abs(deltaMagnitudeDiff) > 0.3f)
            {
                float changeValue = -deltaMagnitudeDiff * 0.05f;
                if(transform.localScale.x + changeValue > 0.25f && transform.localScale.x + changeValue < 5f)
                    transform.localScale += new Vector3(changeValue,changeValue,changeValue);
            }
        }
    }

    /// <summary>
    /// Rotation method
    /// </summary>
    /// <param name="touch"></param>
    void Rotate(Touch touch)
    {
        if ((touch = Input.GetTouch(0)).phase == TouchPhase.Moved)
        {
            Vector2 moved = touch.deltaPosition;
            float rot = Mathf.Deg2Rad * (-moved.x * 10);
            rotated += rot / 6f;
            if (Precision)
            {
                if (Mathf.Abs(rotated) >= PrecisionDegress * Mathf.Deg2Rad)
                {
                    rot = rotated > 0 ? PrecisionDegress : -PrecisionDegress;
                    transform.Rotate(Vector3.up, rot);
                    rotated = 0;
                }
            }
            else
            {
                transform.Rotate(Vector3.up, rot);
                rotated = 0;
            }

        }
    }

    /// <summary>
    /// Assigns new state to currentState
    /// </summary>
    /// <param name="state"></param>
    public void SetState(EditableObject.ObjectState state)
    {
        currentState = state;
        if(currentState == EditableObject.ObjectState.Idle)
        {
            Precision = false;
            PrecisionDegress = 0;
        }
            
    }

    /// <summary>
    /// Toggles precision on rotation
    /// </summary>
    public void TogglePrecision()
    {
        Precision = Precision ? false : true;
    }

    /// <summary>
    /// Assigns controller for the movement 
    /// </summary>
    /// <param name="controller"></param>
    public void AssignController(RoomHandler controller)
    {
        this.controller = controller;
    }

}
