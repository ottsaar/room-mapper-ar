﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditableObject : MonoBehaviour {
    
    public enum ObjectState { Moving, Idle, Rotation, Coloring, Resizing,Editing };
    public static ObjectState currentState;
    //UI elements, assigned by mARController
    public CUIColorPicker colorPicker;
    public GameObject rotationTools;
    public GameObject ToolBar;

    private ToolBarHandler toolBarHandler;
    protected ObjectMovementController movementController;

    /// <summary>
    /// used by other classes in the same gameObject
    /// </summary>
    protected RoomHandler controller;

    void Start()
    {
        movementController = GetComponent<ObjectMovementController>();
        currentState = ObjectState.Idle;
    }

    /// <summary>
    /// Selected this instance
    /// </summary>
    public virtual void Selected()
    {
        movementController = GetComponent<ObjectMovementController>();
        movementController.AssignController(controller);
        toolBarHandler = ToolBar.GetComponent<ToolBarHandler>();
    }

    /// <summary>
    /// Deselected this instance.
    /// </summary>
    public virtual void Deselected()
    {
        currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    /// Toggled button is tinted grayer, this method resets all Buttons
    /// </summary>
    protected void ResetToggleButtons()
    {
        Image[] buttons = ToolBar.GetComponentsInChildren<Image>();
        foreach (Image btn in buttons)
        {
            btn.color = Color.white;
        }
        if(rotationTools == null)
            return;
        buttons = rotationTools.GetComponentsInChildren<Image>();
        foreach (Image btn in buttons)
        {
            btn.color = Color.white;
        }
        rotationTools.GetComponentInChildren<InputField>().GetComponentInChildren<Text>().text = "0";
        rotationTools.GetComponentInChildren<InputField>().interactable = false;
        rotationTools.GetComponentInChildren<Toggle>().isOn = false;
    }

    /// <summary>
    /// Changes Furniture related conditions according to state
    /// </summary>
    protected void ActivateConditions()
    {
        ResetToggleButtons();
        //default values for parameters
        EnableCollision(true);
        if (rotationTools != null)
            rotationTools.SetActive(false);
        if(colorPicker != null)
            colorPicker.gameObject.SetActive(false);
        if (GetComponent<Wall>() != null)
            GetComponent<Wall>().EnableVertices(false);

        //Assigning and changing parameters according to currentState 
        if (currentState == ObjectState.Moving)
        {
            toolBarHandler.MoveButton.GetComponent<Image>().color = Color.gray;
            movementController.SetState(currentState);
        }
        else if (currentState == ObjectState.Rotation)
        {
            toolBarHandler.RotationButton.GetComponent<Image>().color = Color.gray;
            rotationTools.SetActive(true);
            movementController.SetState(currentState);
        }
        else if (currentState == ObjectState.Coloring)
        {
            toolBarHandler.ColorButton.GetComponent<Image>().color = Color.gray;
            colorPicker.gameObject.SetActive(true);
            movementController.SetState(currentState);
        }
        else if (currentState == ObjectState.Editing)
        {
            toolBarHandler.EditButton.GetComponent<Image>().color = Color.gray;
            movementController.SetState(currentState);
        }
        else if (currentState == ObjectState.Resizing)
        {
            toolBarHandler.SizeButton.GetComponent<Image>().color = Color.gray;
            movementController.SetState(currentState);
        }
        else
        {
            movementController.SetState(currentState);

        }
    }

    /// <summary>
    /// Toggles the Collider for movement
    /// </summary>
    public void ToggleMovement()
    {
        if (currentState != ObjectState.Moving)
            currentState = ObjectState.Moving;
        else
            currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    /// since not all editable objects have boxCollider
    /// it gets turned on/off by this field, depending which one the current one has
    /// </summary>
    /// <param name="enable"></param>
    public void EnableCollision(bool enable)
    {
        if (gameObject.GetComponent<BoxCollider>() != null)
            gameObject.GetComponent<BoxCollider>().enabled = enable;
        if (gameObject.GetComponent<MeshCollider>() != null)
            gameObject.GetComponent<MeshCollider>().enabled = enable;
    }

    /// <summary>
    /// Toggles the Collider for movement
    /// </summary>
    public void ToggleEditing()
    {
        if (currentState != ObjectState.Editing)
            currentState = ObjectState.Editing;
        else
            currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    /// Toggles rotation
    /// </summary>
    public void ToggleRotation()
    {
        if (currentState != ObjectState.Rotation)
            currentState = ObjectState.Rotation;
        else
            currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    /// Toggles coloring
    /// </summary>
    public void ToggleColoring()
    {
        if (currentState != ObjectState.Coloring)
            currentState = ObjectState.Coloring;
        else
            currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    /// Toggles resizing
    /// </summary>
	public void ToggleResizing()
    {
        if (currentState != ObjectState.Resizing)
            currentState = ObjectState.Resizing;
        else
            currentState = ObjectState.Idle;
        ActivateConditions();
    }

    /// <summary>
    ///for checking if the object is movable,rotatable..
    /// </summary>
    /// <returns>The current state.</returns>
    public ObjectState GetCurrentState()
    {
        return currentState;
    }

    /// <summary>
    /// Assigns the rotator precision.
    /// </summary>
    /// <param name="angle">angle that the object is allowed to turn on</param>
    public void AssignRotatorPrecision(float angle)
    {
        movementController.PrecisionDegress = angle;
    }

    /// <summary>
    /// Toggles the rotator precision.
    /// </summary>
    public void ToggleRotatorPrecision()
    {
        movementController.TogglePrecision();
    }

    /// <summary>
    /// Makes the object to look at camera turning rotation method.
    /// </summary>
    public void LookAtCamera()
    {
        movementController.lookAtCamera = true;
    }

    /// <summary>C:\Users\Ottsaar1\Desktop\Kool\Bakatoo\room-mapper-ar\Assets\Scripts\FurnitureRelated\EditableObject.cs
    /// Assigns the mARController
    /// </summary>
    public void AssignController(RoomHandler controller)
    {
        this.controller = controller;
    }

    /// <summary>
    /// enables/disables movement controller
    /// </summary>
    public void EnableMovementController(bool enable)
    {
        movementController.enabled = enable;
    }
}
