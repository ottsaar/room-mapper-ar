﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FurnitureSpawner : MonoBehaviour, IDropHandler{
    private RoomHandler controller;
    private TutorialHandler tutorial;
    // Use this for initialization
    void Start()
    {
        controller = FindObjectOfType<RoomHandler>();
        tutorial = FindObjectOfType<TutorialHandler>();
    }
    /// <summary>
    /// Called upon "Dropping" aka letting go of selected object.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        
        if (eventData.selectedObject == null)
            return;
        RectTransform dropArea = transform as RectTransform;
        if (RectTransformUtility.RectangleContainsScreenPoint(dropArea, eventData.position))
        {

            GameObject obj = eventData.selectedObject;
            if (obj.GetComponent<MenuFurnitureItem>() != null)
            {
                RaycastHit hit;
                Ray ray = controller.SelectedCamera.ScreenPointToRay(eventData.position);
                if (Physics.Raycast(ray, out hit))
                {
                    GameObject go = Instantiate(obj.GetComponent<MenuFurnitureItem>().prefab, hit.point, Quaternion.identity);
                    go.transform.parent = controller.SelectedRoom.transform;
                    controller.SelectedRoom.GetComponent<Room>().AddFurniture(go.GetComponent<Furniture>());
                    tutorial.ClickedOn("fs");
                }
            }
            eventData.selectedObject = null;
        }
    }

}
