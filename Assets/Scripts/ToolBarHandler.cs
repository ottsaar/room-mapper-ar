﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBarHandler : MonoBehaviour {
    public enum SelectedObject {Furniture,Wall,Room,None };

    private SelectedObject currentlySelected = SelectedObject.None;

    public GameObject MoveButton;
    public GameObject RotationButton;
    public GameObject SizeButton;
    public GameObject EditButton;
    public GameObject ColorButton;
    public GameObject DeleteButton;

    /// <summary>
    /// Handles the toolbar toggling for different conditions
    /// </summary>
    public void ToggleToolbar()
    {
        DisableAll();
        if(currentlySelected == SelectedObject.Furniture)
        {
            MoveButton.SetActive(true);
            RotationButton.SetActive(true);
            SizeButton.SetActive(true);
            ColorButton.SetActive(true);
            DeleteButton.SetActive(true);
        }
        else if(currentlySelected == SelectedObject.Wall)
        {
            MoveButton.SetActive(true);
            RotationButton.SetActive(true);
            SizeButton.SetActive(true);
            EditButton.SetActive(true);
            DeleteButton.SetActive(true);
        }
        else if(currentlySelected == SelectedObject.Room)
        {
            MoveButton.SetActive(true);
            RotationButton.SetActive(true);
            EditButton.SetActive(true);
            DeleteButton.SetActive(true);
        }
        else
        {
            currentlySelected = ToolBarHandler.SelectedObject.None;
        }
    }
    
    public void SetSelected(SelectedObject selected)
    {
        currentlySelected = selected;
    }

    public void SetAndToggleSelected(SelectedObject selected)
    {
        SetSelected(selected);
        ToggleToolbar();
    }

    public void DisableAll()
    {
        MoveButton.SetActive(false);
        RotationButton.SetActive(false);
        SizeButton.SetActive(false);
        EditButton.SetActive(false);
        ColorButton.SetActive(false);
        DeleteButton.SetActive(false);
    }
}
