﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjToRay : MonoBehaviour
{
    public GameObject HeightIndicator;
    public GameObject HeightIndicator2;
    /// <summary>
    /// objects that are used in making meshes
    /// use cursor as reference
    /// </summary>
    public GameObject clickHandler;
    public GameObject doorPlacer;

    public GameObject cursorRef;
    public GameObject objRef;
	//cursor object that takes the position of objToMove
	public static GameObject cursor;
	//object that is being moved on planes (object for creating points)
    public static GameObject objToMove;
    public static bool putOn = true;
    public static bool OnWalls = false;
	private static Vector3 lastPos;
    public float diff;
    private Vector3 snappingPos = Vector3.positiveInfinity;
    private PlaneCreator creator;
    private TutorialHandler tutorial;
    void Start()
    {
        cursor = cursorRef;
        objToMove = objRef;
        creator = FindObjectOfType<PlaneCreator>();
        doorPlacer.SetActive(false);
        tutorial = FindObjectOfType<TutorialHandler>();
    }

    void Update()
    {
        RaycastHit hit;
        Ray forwardRay = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(forwardRay, out hit))
        {
			if (putOn && hit.collider.tag == "Detectable")
            {
                objToMove.transform.position = hit.point;
                cursor.transform.eulerAngles = new Vector3(0, 180, 0);
                if (hit.collider.GetComponent<Wall>() != null)
                {
                    cursor.transform.eulerAngles = new Vector3(0, 0, 0);
                    doorPlacer.SetActive(true);
                    doorPlacer.GetComponent<DoorPlacer>().cursor = cursor;
                    clickHandler.SetActive(false);
                    OnWalls = true;
                }
                else
                {
                    OnWalls = false;
                    clickHandler.SetActive(true);
                    
                    int size = creator.PlacedVertices.Count;
                    DottedLineRenderer.permission = false;
                    if (size >= 2)
                    {
                        DottedLineRenderer.permission = true;
                        DottedLineRenderer.lastPosition = creator.PlacedVertices[size - 2];
                    }

                    if(doorPlacer.activeInHierarchy){
                        doorPlacer.GetComponent<DoorPlacer>().Reset();
                        doorPlacer.SetActive(false);
                        DottedLineRenderer.doorShape = false;
                    }    
                }
            }
            if (creator.PlacedVertices.Count == 1)
            {
                DottedLineRenderer.permission = true;
                DottedLineRenderer.lastPosition = creator.PlacedVertices[0];
            }
        }


        Vector3 temp = cursor.transform.position;
		if(putOn)
        {
            Vector3 newPos = objToMove.transform.position;
            Vector3 startPos = DottedLineRenderer.lastPosition;
            List<Vector3> placedVerts = creator.PlacedVertices;
            int size = placedVerts.Count;
            Vector3 wayVector = Vector3.zero;
            if(size >= 4)
                 wayVector = (placedVerts[size-2] - placedVerts[size-4]).normalized;

            Vector3 tempVec = Vector3.positiveInfinity;
            if (!(tempVec=snappingBetween(newPos,startPos,wayVector,4)).Equals( Vector3.positiveInfinity) && !OnWalls && size>= 4) 
            {
                snappingPos = tempVec; 
                newPos = snappingPos;
            }
            else
            {
                newPos = objToMove.transform.position;
                snappingPos = Vector3.positiveInfinity;
            }
            cursor.transform.position = newPos;
            HeightIndicator.SetActive(false);
            HeightIndicator2.SetActive(false);
            if (creator.PlacedVertices.Count == 2)
                HeightIndicator2.SetActive(true);
		}
		else
        {
            //keeps the x and z of the point same, only changing y axis.
			objToMove.transform.position = new Vector3 (lastPos.x, objToMove.transform.position.y, lastPos.z);
			cursor.transform.position = objToMove.transform.position;
			cursor.transform.eulerAngles = new Vector3(0,180,180);
            HeightIndicator.SetActive(true);
		}

        //if cursor has been moved
        if(temp != cursor.transform.position)
        {
            DottedLineRenderer.DrawLine(cursor.transform.position);
        }
    }

    /// <summary>
    /// returns value if the angle between a and b is 0,45,90,135,180
    /// with snappingFactor.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns>float</returns>
    Vector3 snappingBetween(Vector3 a, Vector3 startPoint,Vector3 wayVector, float snappingFactor)
    {
        //direction from start to a
        Vector3 directionVec = (a - startPoint);
        Vector3 norm = directionVec.normalized;
        norm.y = 0;
        wayVector.y = 0;
        //angle between them
        float angle = Vector3.SignedAngle(directionVec.normalized, wayVector,Vector3.up);
        for (float i = 0; i < (180+1); i += 45)
        {
            if (i - snappingFactor <= Mathf.Abs(angle) && Mathf.Abs(angle) <= i + snappingFactor)
            {
                //difference(how much is needed to turn, to snap to i degree)
                float diff =-(Mathf.Abs(angle) - i);
                if(angle < 0 )
                    diff = -diff;
                directionVec = rotByAngle(diff,directionVec);
                return directionVec + startPoint;//directionVector back to point a
            }
        }
        return Vector3.positiveInfinity;
    }

    /// <summary>
    /// rotates vector by given degree
    /// </summary>
    /// <param name="degree"></param>
    /// <param name="original"></param>
    /// <returns></returns>
    Vector3 rotByAngle(float degree, Vector3 original)
    {
        return new Vector3(Mathf.Cos(Mathf.Deg2Rad*degree) * original.x - Mathf.Sin(Mathf.Deg2Rad * degree) * original.z, 0, Mathf.Sin(Mathf.Deg2Rad * degree) * original.x + Mathf.Cos(Mathf.Deg2Rad * degree) * original.z);
    }

    /// <summary>
    /// changes the putOn from false to true or otherway..
    /// </summary>
    public static void DoPut()
    {
		if (putOn) 
			lastPos = objToMove.transform.position;
        putOn = putOn ? false : true;
    } 
}