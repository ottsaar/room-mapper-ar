﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

public class DoorWindowObject : Wall {
    //used for saving data
    public DoorData doorData = new DoorData();

    /// <summary>
    /// Creates the door when called
    /// </summary>
    public void CreateDoor(GameObject parent)
    {
        //Can't create door without doorData..
        if (doorData == null)
            return;

        if (Vertices.Count == 0)
            LoadData();
        PlaneCreator planeCreator = FindObjectOfType<PlaneCreator>();
        Vector3 givenPos = parent.transform.position;
        if(doorData.corners.Count != 0)
            givenPos = new Vector3(doorData.posX, doorData.posY, doorData.posZ);

        planeCreator.PlacedVertices = Vertices;
        planeCreator.CreateMesh(gameObject,givenPos);
        if(parent.GetComponent<Room>() != null)
            parent.GetComponent<Room>().AddDoor(GetComponent<DoorWindowObject>());

        transform.parent = parent.transform;
        transform.localEulerAngles = new Vector3(doorData.rotX, doorData.rotY, doorData.rotZ);
        transform.localPosition = givenPos;

        planeCreator.EmptyVertices();
    }
    
    private void UpdateVertices()
    {
        Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
        for(int i = 0; i < Vertices.Count; i++)
        {
            Vertices[i] = verts[i] + transform.localPosition;
        }
    }

    public void LoadData()
    {
        foreach (Corner c in doorData.corners)
        {
            Vertices.Add(new Vector3(c.posX, c.posY, c.posZ));
        }
    }

    public void StoreData()
    {
        UpdateVertices();

        doorData.corners.Clear();
        Vector3 pos = transform.position;
        Vector3 rot = transform.eulerAngles;

        doorData.posX = pos.x;
        doorData.posY = pos.y;
        doorData.posZ = pos.z;

        doorData.rotX = rot.x;
        doorData.rotY = rot.y;
        doorData.rotZ = rot.z;

        foreach (Vector3 vert in Vertices)
        {
            Corner corner = new Corner();
            corner.posX = vert.x;
            corner.posY = vert.y;
            corner.posZ = vert.z;

            doorData.corners.Add(corner);
        }
    }
}
public class DoorData
{
    [XmlElement("PosX")]
    public float posX;
    [XmlElement("PosY")]
    public float posY;
    [XmlElement("PosZ")]
    public float posZ;

    [XmlElement("RotX")]
    public float rotX;
    [XmlElement("RotY")]
    public float rotY;
    [XmlElement("RotZ")]
    public float rotZ;

    [XmlArray("Corners")]
    [XmlArrayItem("Corner")]
    public List<Corner> corners = new List<Corner>();
}
