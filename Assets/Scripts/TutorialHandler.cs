using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHandler: MonoBehaviour
{
    public bool showTutorial = false;

    public GameObject WelcomeMessage;
    public GameObject ButtonInfo;
    public GameObject ButtonInfo2;
    public GameObject ButtonInfo3;
    public GameObject ButtonInfo4;
    public GameObject MenuInfo;
    public GameObject EditingMode;
    public GameObject DoorInfo;
    public GameObject SelectionInfo;

    public GameObject exportSuccess;
    public GameObject exportFailed;

    public string shownMenu;
    public Image loader;
    public GameObject closeButton;
    private float showTime;
    private List<string> shown = new List<string>();

    void Start()
    {
        showTutorial = FindObjectOfType<AppController>().tutorial;
        ClickedOn("w");
    }
    void Update()
    {

    }

    public void ClickedOn(string message)
    {
        if (!showTutorial)
            return;
        if (shown.Contains(message))
        {
            return;
        }
        DisableAll();

        switch (message)
        {
            case "w":
                WelcomeMessage.SetActive(true);
                break;
            case "b1":
                ButtonInfo.SetActive(true);
                break;
            case "b2":
                ButtonInfo2.SetActive(true);
                break;
            case "b3":
                ButtonInfo3.SetActive(true);
                break;
            case "b4":
                ButtonInfo4.SetActive(true);
                break;
            case "m":
                MenuInfo.SetActive(true);
                break;
            case "e":
                EditingMode.SetActive(true);
                break;
            case "fs":
                SelectionInfo.SetActive(true);
                break;
            default:
                DisableAll();
                break;
        }
        if (!message.Equals(""))
        {
            shown.Add(message);
            closeButton.SetActive(true);
        }
        shownMenu = message;
    }

    /// <summary>
    /// changes success description
    /// </summary>
    /// <param name="name"></param>
    public void ChangeSuccessDescription(string name)
    {
        exportSuccess.GetComponentInChildren<Text>().text = "Export successful!\nfile saved as " + name + ".obj";
    }

    public void ExportSuccess()
    {
        exportSuccess.SetActive(true);
        showTime = Time.time;
        closeButton.SetActive(true);
    }

    public void ExportFailed()
    {
        exportFailed.SetActive(false);
        showTime = Time.time;
        closeButton.SetActive(true);
    }
   
    public void DisableAll()
    {
        WelcomeMessage.SetActive(false);
        ButtonInfo.SetActive(false);
        ButtonInfo2.SetActive(false);
        ButtonInfo3.SetActive(false);
        ButtonInfo4.SetActive(false);
        MenuInfo.SetActive(false);
        loader.gameObject.SetActive(false);
        closeButton.SetActive(false);
        exportFailed.SetActive(false);
        exportSuccess.SetActive(false);
        EditingMode.SetActive(false);
        DoorInfo.SetActive(false);
        SelectionInfo.SetActive(false);
    }

}