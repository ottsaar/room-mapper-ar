﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeChecker : MonoBehaviour {
    private RoomHandler controller;
    private Text text;
    void Start()
    {
        text = transform.GetComponent<Text>();
    }

    /// <summary>
    /// on call checks the current editing mode
    /// and changes the text of the parent
    /// </summary>
    public void CheckMode()
    {
        if (controller == null)
            controller = FindObjectOfType<RoomHandler>();
        if (controller.IsEditingMode())
        {
           text.text = "AR Mode";
           FindObjectOfType<TutorialHandler>().ClickedOn("e");
        }
        else
        {
           text.text = "Editing Mode";
        }
    }
    
    public void AssignController(RoomHandler controller)
    {
        this.controller = controller;
    }
}
