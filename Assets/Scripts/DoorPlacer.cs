﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorPlacer : MonoBehaviour {
    public GameObject cursor;
    Vector3[] vertices = new Vector3[2];
    public GameObject doorPrefab;
    public Sprite firstCorner;
    public Sprite secondCorner;
    private RoomHandler roomHandler;
    public PlaneCreator creator;

    private bool placed = false;

    void Start()
    {
        creator = FindObjectOfType<PlaneCreator>();
        roomHandler = FindObjectOfType<RoomHandler>();
    }

    public void PlaceVertex()
    {
        if (!placed)
        {
            vertices[0] = cursor.transform.position;
            DottedLineRenderer.lastPosition = vertices[0];
            DottedLineRenderer.permission = true;
            DottedLineRenderer.doorShape = true;
            placed = true;
            GetComponentInChildren<Image>().sprite = secondCorner;
        }
        else
        {
            vertices[1] = cursor.transform.position;

            Vector3 point2 = new Vector3(vertices[0].x, vertices[0].y, vertices[1].z);
            Vector3 point3 = new Vector3(vertices[0].x, vertices[1].y, vertices[0].z);
            List<Vector3> verticesList = new List<Vector3>();
            verticesList.Add(vertices[0]);
            verticesList.Add(vertices[1]);

            //resetting information
            DottedLineRenderer.permission = false;
            DottedLineRenderer.doorShape = false;
            //sending to mesh creator
            Vector3 p1 = new Vector3(vertices[1].x - (vertices[1].x - vertices[0].x )/2, vertices[0].y, vertices[1].z - (vertices[1].z - vertices[0].z )/2);
            Vector3 center = p1 - (p1 - vertices[0]) / 2;
            GameObject obj = Instantiate(doorPrefab, center, Quaternion.identity);
            creator.CreateMesh(obj, center, verticesList);

            roomHandler.SelectedRoom.GetComponent<Room>().AddDoor(obj.GetComponent<DoorWindowObject>());
            obj.transform.parent = roomHandler.SelectedRoom.transform;

            //empty vertices array
            vertices = new Vector3[2];
            placed = false;
            Reset();
        }
    }
    public void Reset(){
        vertices = new Vector3[2];
        GetComponentInChildren<Image>().sprite = firstCorner;
    }
}
