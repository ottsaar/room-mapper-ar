﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EditorCameraController : MonoBehaviour {
    public static bool allowMovement = true;
    public float perspectiveZoomSpeed = 0.05f;
    public Text text;
    //Camera object that is showing the room
    private Camera usedCamera;
    private Vector3 moveTo;
    private bool moving = false;
    private bool rotating = false;
    private bool zooming = false;
    private float camMoveSpeed = 5;
    // Use this for initialization
    void Start () {
        usedCamera = GetComponentInChildren<Camera>();
	}

    // Update is called once per frame
    void Update()
    {
        //moves the point to the assigned point
        if (moving)
        {
            transform.position = Vector3.Lerp(transform.position, moveTo, Time.deltaTime * camMoveSpeed);
            if (transform.position == moveTo)
                moving = false;
        }
        if(rotating)
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,new Vector3(45,0,0), Time.deltaTime * camMoveSpeed);
            if(transform.eulerAngles == new Vector3(45,0,0))
                rotating = false;
        }
        if(zooming)
        {
            usedCamera.fieldOfView = Mathf.Lerp(usedCamera.fieldOfView,100,Time.deltaTime*camMoveSpeed);
            if((int)usedCamera.fieldOfView == 100)
                zooming = false;
        }
        //if movement isn't allowed end update.
        if (!allowMovement)
        {
            return;
        }

        Touch touch;
        //if no touch then end update
        if (Input.touchCount < 1)
        {
            return;
        }
        //Moving with one finger causes rotation of the camera around the reference point
        if ((touch = Input.GetTouch(0)).phase == TouchPhase.Moved && Input.touchCount == 1)
        {
            //interupts the reseting of camera
            ResetCamera(false);

            Vector2 moved = touch.deltaPosition;
            float rotx = Mathf.Deg2Rad * moved.x * 10;
            float roty = Mathf.Deg2Rad * -moved.y * 10;
            float newX = Mathf.Clamp(transform.eulerAngles.x + roty, 3.0f, 89.5f);
            //newAngles for the tranfrom after rotating
            Vector3 newAngles = new Vector3(newX, transform.eulerAngles.y + rotx, transform.eulerAngles.z);
            transform.eulerAngles = newAngles;

        }
        bool zoomed = false;
        //Zooming
        if (Input.touchCount == 2)
        {
            //interrupts the reseting of camera
            ResetCamera(false);

            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (Mathf.Abs(deltaMagnitudeDiff) > 5f)
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                usedCamera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                usedCamera.fieldOfView = Mathf.Clamp(usedCamera.fieldOfView, 5f, 100f);
                zoomed = true;
            }
        }
        //Moving with two fingers causes the EditorCamera Object to move.
        if ((touch = Input.GetTouch(0)).phase == TouchPhase.Moved && Input.touchCount >= 2 && !zoomed)
        {
            //interupts the reseting of camera
            ResetCamera(false);

            Vector2 moved = touch.deltaPosition;
            Vector3 moveVector = new Vector3(-Mathf.Deg2Rad * moved.x, 0, Mathf.Deg2Rad * -moved.y);

            if(transform.forward == Vector3.back)
            {
                moveVector.x = -moveVector.x;
            }
            else if(transform.forward == Vector3.left)
            {
                float temp = moveVector.x;
                moveVector.x = -moveVector.y;
                moveVector.y = temp;
            }
            else if(transform.forward == Vector3.right)
            {
                float temp = moveVector.x;
                moveVector.x = moveVector.y;
                moveVector.y = temp;
            }
            transform.position = transform.position + (Quaternion.Euler(0,transform.eulerAngles.y,0) * moveVector);
        }
    }

    public void AllowMovement(bool value)
    {
        allowMovement = value;
    }

    public bool GetMovementBool()
    {
        if (allowMovement)
            return true;
        return false;
    }

    /// <summary>
    /// sets the point the camera moves to.
    /// </summary>
    /// <param name="point"></param>
    public void MoveTo(Vector3 point)
    {
        moveTo = point;
        moving = true;
    }

    /// <summary>
    /// Resets the cameras position
    /// </summary>
    public void ResetCamera(bool enabled)
    {
        moveTo = new Vector3(0,0,0);
        moving = enabled;
        rotating = enabled;
        zooming = enabled;
    }
}
