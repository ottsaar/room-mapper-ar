﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchHandler : MonoBehaviour {

    public GameObject furnitureObjects;
	
    public void SeachFor(string search)
    {
        search = search.Replace(" ", "").ToLower();
        if (search.Equals(""))
        {
            ShowAll();
            return;
        }

        for(int i = 0;i < furnitureObjects.transform.childCount;i++)
        {
            Transform slot = furnitureObjects.transform.GetChild(i);
            if (slot.GetComponentInChildren<MenuFurnitureItem>())
            {
                bool result = false;
                string[] found = slot.GetComponentInChildren<MenuFurnitureItem>().name.Split('|');
                foreach(string name in found)
                {
                    if (name.ToLower().StartsWith(search))
                    {
                        result = true;
                        break;
                    }
                }
                slot.gameObject.SetActive(result);
            }
            else
            {
                slot.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// shows all the slots
    /// </summary>
    void ShowAll()
    {
        for (int i = 0; i < furnitureObjects.transform.childCount; i++)
        {
            Transform slot = furnitureObjects.transform.GetChild(i);
            slot.gameObject.SetActive(true);
        }
    }
}
