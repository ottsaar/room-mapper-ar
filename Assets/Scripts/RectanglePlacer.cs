﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RectanglePlacer : MonoBehaviour {
    public GameObject emptyPrefab;
    public GameObject empty;
    public GameObject empty2;
    public Image image;
    public GameObject confirmer;
    public Color color;
    public GameObject doorPrefab;
    public Canvas canvas;
    private RoomHandler roomHandler;
    private PlaneCreator creator;
    private Vector2 confCenter;

    void Start()
    {
        creator = FindObjectOfType<PlaneCreator>();
    }

    /// <summary>
	/// takes List of rectangles as input and raycasts them on to wall.
	/// </summary>
	public void ShowingCorners(List<Vector2[]> rectangles)
    {
        if (roomHandler == null)
            roomHandler = FindObjectOfType<RoomHandler>();
        Clear();
        empty = Instantiate(emptyPrefab, Vector3.zero,Quaternion.identity);
        empty2 = Instantiate(emptyPrefab,transform);
        foreach (Vector2[] rectangle in rectangles)
        {
            List<Vector3> vertices = new List<Vector3>();
            for (int i = 0; i < 4; i++)
            {
                var centreX = rectangle[i].x * canvas.scaleFactor;
                var centreY = rectangle[i].y * canvas.scaleFactor;
                Image img = Instantiate(image,empty2.transform);
                img.rectTransform.anchoredPosition = new Vector2(centreX,centreY);
                if(i == 1)
                    confCenter.y = centreY - (centreY - confCenter.y)/2;
                if(i == 0)
                    confCenter = new Vector2(centreX,centreY);
                else if(i == 2)
                {
                    confCenter.x = centreX - (centreX - confCenter.x) / 2;
                }
                float scHeight = Screen.height;
                Ray ray = Camera.main.ScreenPointToRay(new Vector2(centreX , centreY));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.GetComponent<Wall>() != null)
                    {
                        vertices.Add(hit.point);
                    }
                }
            }
            if (vertices.Count == 4)
            {
                Vector3 centerOfVerts = vertices[2] - (vertices[2] - vertices[0]) / 2;
                GameObject go = Instantiate(doorPrefab, centerOfVerts, Quaternion.identity);
                List<Vector3> oldVerts = creator.PlacedVertices;
                creator.PlacedVertices = vertices;
                creator.CreateMesh(go, centerOfVerts);
                creator.PlacedVertices = oldVerts;
                go.transform.parent = empty.transform;
                GameObject conf = Instantiate(confirmer,empty2.transform);
                conf.GetComponent<Image>().rectTransform.anchoredPosition = confCenter;
                conf.GetComponent<DoorConfirmer>().refDoor = go.GetComponent<DoorWindowObject>();
            }
        }
	}

    /// <summary>
	/// Clears the scene from all temporarely added objects
	/// </summary>
    public void Clear()
    {
        if (empty != null)
        {
            Destroy(empty);
            Destroy(empty2);
        } 
    }
}
