﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

public class Room : EditableObject
{
    public RoomData roomData = new RoomData();
    public GameObject HighlightBox;
    public GameObject prefabWall;
    public GameObject prefabDoor;
    public List<Wall> wallsList = new List<Wall>();
    public List<Furniture> furnitureList = new List<Furniture>();
    public List<DoorWindowObject> doorList = new List<DoorWindowObject>();

    private List<WallData> wallDataList = new List<WallData>();
    private List<FurnitureData> furnitureDataList = new List<FurnitureData>();
    public List<DoorData> doorDataList = new List<DoorData>();

    private Vector3 posToBeAssigned;
    private Vector3 rotToBeAssigned;


    void Start()
    {
        currentState = ObjectState.Idle;
    }
    public void Selected(){
        base.Selected();
        HighlightBox.SetActive(true);
    }
    public void Deselected(){
        base.Deselected();
        HighlightBox.SetActive(false);
        GetComponent<BoxCollider>().enabled = true;
    }
    public void ToggleEditing()
    {
        base.ToggleEditing();
        EnableChildren();
        FindObjectOfType<ProjectHandler>().EditSelectedRoom();
    }

    /// <summary>
    /// Enables all the collisions for children
    /// </summary>
    void EnableChildren(){
        foreach(Furniture furniture in furnitureList)
        {
            furniture.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
        foreach(Wall wall in wallsList)
        {
            wall.gameObject.GetComponent<MeshCollider>().enabled = true;
        }
        foreach(DoorWindowObject door in doorList)
        {
            door.gameObject.GetComponent<MeshCollider>().enabled = true;
        }
    }

    ///Deletes object from the list
    public void DeleteObject(GameObject obj)
    {
        if(obj.GetComponent<DoorWindowObject>() != null)
        {
            doorList.Remove(obj.GetComponent<DoorWindowObject>());
        }
        else if(obj.GetComponent<Wall>() != null)
        {
            wallsList.Remove(obj.GetComponent<Wall>());
        }
        else if(obj.GetComponent<Furniture>() != null)
        {
            furnitureList.Remove(obj.GetComponent<Furniture>());
        }

    }
    /// <summary>
    /// Add wall to the wallsList
    /// </summary>
    /// <param name="wall"></param>
    public void AddWall(Wall wall)
    {
        wallsList.Add(wall);
    }

    public void RemoveWall(Wall wall)
    {
        wallsList.Remove(wall);
    }

    /// <summary>
    /// Add furniture object to furnitureList
    /// </summary>
    /// <param name="furniture"></param>
    public void AddFurniture(Furniture furniture)
    {
        furnitureList.Add(furniture);
    }

    public void RemoveFurniture(Furniture furniture)
    {
        furnitureList.Remove(furniture);
    }

    /// <summary>
    /// Add door object to doorList
    /// </summary>
    /// <param name="door"></param>
    public void AddDoor(DoorWindowObject door)
    {
        doorList.Add(door);
    }

    public void RemoveDoor(DoorWindowObject door)
    {
        doorList.Remove(door);
    }

    public void CreateRoom()
    {
        if (roomData.name == "")
            return;
        if (wallDataList.Count == 0)
            LoadData();

        posToBeAssigned.y = FindObjectOfType<PlanePlacer>().transform.position.y;
        print(posToBeAssigned);
        transform.position = posToBeAssigned;
        transform.eulerAngles = rotToBeAssigned;
        foreach (WallData wall in wallDataList)
        {
            GameObject go = Instantiate(prefabWall, transform.position, Quaternion.identity);
            go.GetComponent<Wall>().wallData = wall;
            go.GetComponent<Wall>().CreateWall(gameObject);

        }

        foreach (DoorData door in doorDataList)
        {
            GameObject go = Instantiate(prefabDoor, transform.position, Quaternion.identity);
            go.GetComponent<DoorWindowObject>().doorData = door;
            go.GetComponent<DoorWindowObject>().CreateDoor(gameObject);
        }

        foreach (FurnitureData furniture in furnitureDataList)
        {
            print(furniture.name);
            GameObject prefab = Resources.Load("Prefabs/Furniture/" + furniture.name) as GameObject;
            GameObject go = Instantiate(prefab, transform.position, Quaternion.identity);
            go.transform.parent = transform;
            go.GetComponent<Furniture>().furnitureData = furniture;
            go.GetComponent<Furniture>().LoadData();
            AddFurniture(go.GetComponent<Furniture>());

        }

        FitToChildren();//Creates boxcollider around children

    }

    /// <summary>
    /// Storing data into RoomData
    /// </summary>
    public void StoreData()
    {
        roomData.name = transform.name;

        Vector3 position = transform.position;
        roomData.posX = position.x;
        roomData.posY = position.y;
        roomData.posZ = position.z;

        Vector3 angles = transform.eulerAngles;
        roomData.rotX = angles.x;
        roomData.rotY = angles.y;
        roomData.rotZ = angles.z;

        wallDataList.Clear();
        foreach (Wall wall in wallsList)
        {
            wall.StoreData();
            wallDataList.Add(wall.wallData);
        }

        furnitureDataList.Clear();
        foreach (Furniture furniture in furnitureList)
        {
            furniture.StoreData();
            furnitureDataList.Add(furniture.furnitureData);
        }

        doorDataList.Clear();
        foreach (DoorWindowObject door in doorList)
        {
            door.StoreData();
            doorDataList.Add(door.doorData);
        }

        roomData.wallsList = wallDataList;
        roomData.furnitureList = furnitureDataList;
        roomData.doorList = doorDataList;
    }

    public void LoadData()
    {
        transform.name = roomData.name;
        wallDataList = roomData.wallsList;
        furnitureDataList = roomData.furnitureList;
        doorDataList = roomData.doorList;
        posToBeAssigned = new Vector3(roomData.posX, roomData.posY, roomData.posZ);
        rotToBeAssigned = new Vector3(roomData.rotX, roomData.rotY, roomData.rotZ);
    }

    /// <summary>
    /// makes a boxCollider with a size of the boundry around children
    /// </summary>
    public void FitToChildren()
    {
        List<Vector3> allItems = new List<Vector3>();
        if (gameObject.GetComponent<BoxCollider>() == null)
            gameObject.AddComponent<BoxCollider>();

        bool hasBounds = false;
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        foreach (Furniture furniture in furnitureList)
        {
            Renderer childRenderer = furniture.gameObject.GetComponentInChildren<Renderer>();
            if (childRenderer != null)
            {
                if (hasBounds)
                {
                    bounds.Encapsulate(childRenderer.bounds);
                }
                else
                {
                    bounds = childRenderer.bounds;
                    hasBounds = true;
                }
            }
            furniture.gameObject.GetComponent<BoxCollider>().enabled = false;
            allItems.Add(furniture.transform.localPosition);
        }

        foreach (Wall wall in wallsList)
        {
            Renderer childRenderer = wall.gameObject.GetComponent<Renderer>();
            if (childRenderer != null)
            {
                if (hasBounds)
                { 
                    bounds.Encapsulate(childRenderer.bounds);
                    bounds.Encapsulate(wall.transform.position);
                }
                else
                {
                    bounds = childRenderer.bounds;
                    hasBounds = true;
                }
            }
            wall.gameObject.GetComponent<MeshCollider>().enabled = false;
            allItems.Add(wall.transform.localPosition);
        }

        foreach (DoorWindowObject door in doorList) 
        {
            Renderer childRenderer = door.gameObject.GetComponent<Renderer>();
            if (childRenderer != null)
            {
                if (hasBounds)
                {
                    bounds.Encapsulate(childRenderer.bounds);
                }
                else
                {
                    bounds = childRenderer.bounds;
                    hasBounds = true;
                }
            }
            door.gameObject.GetComponent<MeshCollider>().enabled = false;
            allItems.Add(door.transform.localPosition);
        }
        Vector2 bigSmall = new Vector2(float.PositiveInfinity,0);
        Vector3 center = Vector3.zero;
        float distance = 0;
        for(int i = 0;i<allItems.Count;i++)
        {
            for(int j = i+1;j<allItems.Count;j++)
            {
                float temp =Vector3.Distance(allItems[i],allItems[j]);
                if(temp>distance)
                {
                    distance = temp;
                    center = (allItems[i]+allItems[j]) / 2;
                }
            }
            if(bigSmall[0] > allItems[i].y)
                bigSmall[0] = allItems[i].y;
            if(bigSmall[1] < allItems[i].y)
                bigSmall[1] = allItems[i].y;
        }
        if(allItems.Count == 1)
        {
            center = allItems[0];
        }
        BoxCollider collider = (BoxCollider)gameObject.GetComponent<BoxCollider>();
        center.y += bounds.size.y/2;
        collider.center = center;
        collider.size = bounds.size + new Vector3(0.1f,0.1f,0.1f);//for making extra space around selected room.

            //box that is shown, on the selection of the room
        HighlightBox.transform.localScale = bounds.size + new Vector3(0.1f,0.1f,0.1f) ;
        HighlightBox.transform.localPosition = center;
        }
}
public class RoomData
{
        [XmlAttribute("Name")]
        public string name;

        [XmlElement("PosX")]
        public float posX;
        [XmlElement("PosY")]
        public float posY;
        [XmlElement("PosZ")]
        public float posZ;

        [XmlElement("RotX")]
        public float rotX;
        [XmlElement("RotY")]
        public float rotY;
        [XmlElement("RotZ")]
        public float rotZ;

        [XmlArray("Walls")]
        [XmlArrayItem("Wall")]
        public List<WallData> wallsList = new List<WallData>();

        [XmlArray("Furnitures")]
        [XmlArrayItem("Furniture")]
        public List<FurnitureData> furnitureList = new List<FurnitureData>();

        [XmlArray("Doors")]
        [XmlArrayItem("Door")]
        public List<DoorData> doorList = new List<DoorData>();

}
