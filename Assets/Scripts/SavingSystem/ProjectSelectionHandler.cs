﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProjectSelectionHandler : MonoBehaviour {
    public GameObject projectPrefab;
    public GameObject prefab;
    public string newProjectName;
    private AppController appController;
    private List<string> projects = new List<string>();
    private List<string> addedProjects = new List<string>();



    void Start()
    {
        appController = AppController.Instance;
        foreach (ProjectData data in AppController.projectDatas)
        {
            projects.Add(data.name);
        }
        if (projects.Count != 0)
        {
            foreach (string project in projects)
            {
                if (addedProjects.Contains(project))
                    continue;
                GameObject go = Instantiate(prefab, transform);
                Button[] buttons =  go.GetComponentsInChildren<Button>();
                buttons[0].onClick.AddListener(() => OnClicked());
                buttons[0].gameObject.GetComponentInChildren<Text>().text = project;
                buttons[0].gameObject.name = project;
                buttons[1].onClick.AddListener(() => OnDelete());
                buttons[1].gameObject.name = "D!" + project;
                addedProjects.Add(project);
            }
            appController.tutorial = false;
        }
        else
        {
            appController.tutorial = true;
        }
    }

    public void SetNewProjectName(string projectName)
    {
        newProjectName = projectName.ToLower();
    }

    public bool checkProjectAvailability()
    {
        if (newProjectName == "")
            return false;
        foreach(string name in projects)
        {
            if (name == newProjectName)
                return false;
        }
        return true;
    }

    public void CreateNewProject()
    {
        if (checkProjectAvailability())
        {
             appController.CreateNewProject(newProjectName);
        }
        else
        {
            Transform newProjectObject = EventSystem.current.currentSelectedGameObject.transform.parent;
            newProjectObject.GetChild(0).gameObject.SetActive(true);
            if(newProjectName == "")
                newProjectObject.transform.GetChild(0).GetComponent<Text>().text = "project name can't be empty";
            else
                newProjectObject.transform.GetChild(0).GetComponent<Text>().text = "project with that name already exists";
        }
    }

    public void OnDelete()
    {
        string butName = EventSystem.current.currentSelectedGameObject.name.Split('!')[1];
        appController.DeleteProject(butName);
        Destroy(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
        projects.Remove(butName);
    }

    public void OnClicked()
    {
        string butName = EventSystem.current.currentSelectedGameObject.name;
        if (butName != "NewProject")
        {
            appController.OnSelectedProject(butName);
        }
    }
}
