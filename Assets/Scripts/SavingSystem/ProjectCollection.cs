﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("ProjectCollection")]
public class ProjectCollection
{
    [XmlArray("Projects")]
    [XmlArrayItem("Project")]
    public List<ProjectData> projects = new List<ProjectData>();
}
