﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

public class Project : MonoBehaviour
{
    public bool newProject = false;
    public ProjectData projectData = new ProjectData();

    public GameObject roomPrefab;
    public List<Room> rooms = new List<Room>();
    private List<RoomData> roomDatas = new List<RoomData>();
    public Vector3 VirtualPlanePos;

    public void AddRoom(Room room)
    {
        rooms.Add(room);
    }

    public void RemoveRoom(Room room)
    {
        rooms.Remove(room);
    }

    public void SelectedRoom(string name)
    {
        foreach(Room room in rooms)
        {
            if (!room.name.Equals(name))
                room.gameObject.SetActive(false);
            else
                room.gameObject.SetActive(true);
            room.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
    public void ShowAllRooms()
    {
        foreach(Room room in rooms)
        {
            room.gameObject.SetActive(true);
            room.gameObject.GetComponent<BoxCollider>().enabled = true;
            //updates the box collider sizes
            room.FitToChildren();
            room.transform.position = new Vector3(room.roomData.posX, VirtualPlanePos.y, room.roomData.posZ);
        }
    }

    public void LoadData()
    {
        transform.name = projectData.name;
        roomDatas = projectData.Rooms;
    }

    public void StoreData()
    {
        
        VirtualPlanePos = FindObjectOfType<PlanePlacer>().transform.position;
        projectData.vposX = VirtualPlanePos.x;
        projectData.vposY = VirtualPlanePos.y;
        projectData.vposZ = VirtualPlanePos.z;

        projectData.name = this.gameObject.name;
        roomDatas.Clear();
        foreach (Room room in rooms)
        {
            room.StoreData();
            roomDatas.Add(room.roomData);
        }

        projectData.Rooms = roomDatas;

    }

    public void CreateRooms()
    {
        foreach (RoomData roomdata in roomDatas)
        {
            Vector3 pos = new Vector3(roomdata.posX, VirtualPlanePos.y, roomdata.posZ);
            GameObject go = Instantiate(roomPrefab, pos, Quaternion.identity);
            go.GetComponent<Room>().roomData = roomdata;
            go.GetComponent<Room>().LoadData();
            go.GetComponent<Room>().CreateRoom();
            go.transform.parent = transform;
            AddRoom(go.GetComponent<Room>());
        }
    }

    public void OnSelectedProject()
    {
        SaveData.OnLoaded += delegate { LoadData(); };
        SaveData.OnBeforeSave += delegate { StoreData(); };
        SaveData.OnBeforeSave += delegate { SaveData.AddProjectData(projectData); };
        ShowAllRooms();
    }

    public void ClearDelegation()
    {
        SaveData.OnLoaded -= delegate { LoadData(); };
        SaveData.OnBeforeSave -= delegate { StoreData(); };
        SaveData.OnBeforeSave -= delegate { SaveData.RemoveProjectData(projectData); };
    }
}
public class ProjectData
{
    [XmlAttribute("Name")]
    public string name;

    [XmlElement("VPosX")]
    public float vposX;
    [XmlElement("VPosY")]
    public float vposY;
    [XmlElement("VPosZ")]
    public float vposZ;

    [XmlArray("Rooms")]
    [XmlArrayItem("Room")]
    public List<RoomData> Rooms = new List<RoomData>();
}
