﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using GoogleARCore.Examples.BasicCalc;

public class AppController : MonoBehaviour
{

    public static AppController Instance { get; private set; }

    private static string dataPath = "";
    public static List<ProjectData> projectDatas = new List<ProjectData>();

    public ProjectData selectedProjectData;

    public string SelectedProject;
    public bool tutorial = false;

    public bool newProject = false;

    public bool newRoom = false;
    public int count;

    private Scene currentScene;

  


    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        // just move it to the root
        this.transform.parent = null;
        Instance = this;


        DontDestroyOnLoad(gameObject);

        if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            dataPath = System.IO.Path.Combine(Application.persistentDataPath, "projects.xml");
        else
            dataPath = System.IO.Path.Combine(Application.dataPath, "Resources/projects.xml");
    }

    /// <summary>
    /// Creates project from given data
    /// </summary>
    public static void GetProjectData(ProjectData data)
    {
        projectDatas.Add(data);
    }

    private void ResetInfo()
    {         
        newProject = false;
        newRoom = false;
        SelectedProject = "";
        selectedProjectData = null;
    }

    public void ToProjectMenu()
    {
        ResetInfo();
        Load();
        SceneManager.LoadScene("ProjectMenu");
        SaveData.ClearProjectData();
    }
    
    public void ToMainMenu()
    {
        ResetInfo();
        Load();
        SceneManager.LoadScene("MainMenu");
    }

    public void ToRoomCreation()
    {

        Load();
        SceneManager.LoadScene("RoomCreation");
    }
    
    public ProjectData ReturnProject(string name)
    {
    foreach (ProjectData projectData in projectDatas)
        {
            if (projectData.name == name)
            {
                return projectData;
            }
        }
        return null;
    }


    public void DeleteProject(string name)
    {
        foreach (ProjectData projectData in projectDatas)
        {
            if (projectData.name == name)
            {
                print("deleted "+name);
                SaveData.RemoveProjectData(projectData);
                break;
            }
        }
        projectDatas = SaveData.projectCollection.projects;
        Save();
    }

    public void OnSelectedProject(string name)
    {
        foreach(ProjectData projectData in projectDatas)
        {
            if (projectData.name.Equals(name))
            {
                selectedProjectData = projectData;
                SelectedProject = name;
            }  
        }
        ToRoomCreation();
    }

    public void Exit(){
        Application.Quit();
    }
    public void CreateNewProject(string name)
    {
        SelectedProject = name;
        newProject = true;
        ToRoomCreation();
    }

    public void Load()
    {
        SaveData.Load(dataPath);
    }

    public void Save()
    {
        SaveData.Save(dataPath, SaveData.projectCollection);
    }
}
