﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System;

public class SaveData
{
    public static ProjectCollection projectCollection = new ProjectCollection();

    public delegate void SerializeAction();
    public static event SerializeAction OnLoaded;
    public static event SerializeAction OnBeforeSave;

    public static void Load(string path)
    {
        projectCollection = LoadProjects(path);
        foreach (ProjectData data in projectCollection.projects)
        {
            AppController.GetProjectData(data);
        }
    }
    public static void Save(string path, ProjectCollection collection)
    {
        if(OnBeforeSave != null)
        {
            OnBeforeSave();
        }
        //Filtering out duplicates, if in by accident
        List<string> olemas = new List<string>(); 
        ProjectCollection filtered = new ProjectCollection();
        for(int i = projectCollection.projects.Count-1; i >= 0; i--)
        {
            ProjectData data = projectCollection.projects[i];
            if(!olemas.Contains(data.name))
            {
                olemas.Add(data.name);
                filtered.projects.Add(data);
            }
        }
       
        SaveProjects(path, filtered);


        ClearProjectData();
        OnBeforeSave = null;
        OnLoaded = null;
    }

    public static void AddProjectData(ProjectData projectData)
    {
        projectCollection.projects.Add(projectData);
    }

    public static void RemoveProjectData(ProjectData projectData)
    {
        projectCollection.projects.Remove(projectData);
    }

    public static void ClearProjectData()
    {
        projectCollection.projects.Clear();
    }


    private static ProjectCollection LoadProjects(string path)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ProjectCollection));
        FileStream fileStream;
        ProjectCollection projects;
        try
        {
            fileStream = new FileStream(path, FileMode.Open);
            projects = serializer.Deserialize(fileStream) as ProjectCollection;
            fileStream.Close();
        }
        catch(FileNotFoundException e)
        {
            fileStream = new FileStream(path, FileMode.Create);
            projects = new ProjectCollection();
            fileStream.Close();
        }
        catch(XmlException e)
        {
            projects = new ProjectCollection();
        }
        return projects;
    }

    private static void SaveProjects(string path,ProjectCollection projects)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ProjectCollection));
        FileStream fileStream;

        try {
            fileStream = new FileStream(path, FileMode.Truncate);
        }
        catch(FileNotFoundException e)
        {
            fileStream = new FileStream(path, FileMode.Create);
            Debug.Log("ei leidnud fiali");
        }

        serializer.Serialize(fileStream, projects);

        fileStream.Close();
    }
}
