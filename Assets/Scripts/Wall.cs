﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

public class Wall : EditableObject {
    public GameObject spherePrefab;
    public GameObject SelectedSphere;
    public Texture normalTex;
    public Texture selectedTex;
    public List<GameObject> visibleVertices = new List<GameObject>();
    //used for saving data
    public WallData wallData = new WallData();
    
	public List<Vector3> Vertices = new List<Vector3>();
    public void Selected()
    {
        base.Selected();
        GetComponent<Renderer>().material.mainTexture = selectedTex;
    }

    public void Deselected()
    {
        
        base.Deselected();
        GetComponent<Renderer>().material.mainTexture = normalTex;
        EnableVertices(false);
    }

    /// <summary>
    /// toggles editing mode, making spheres visible
    /// </summary>
    public void ToggleEditing()
    {
        base.ToggleEditing();
        if (currentState == ObjectState.Editing)
            EnableVertices(true);
    }

    public void SphereSelected(bool enable,GameObject obj)
    {
        if (enable)
        {
            SelectedSphere = obj;
            SelectedSphere.GetComponent<SphereHandler>().Selected(enable);
        }
        else
        {
            if (SelectedSphere == null)
                return;
            SelectedSphere.GetComponent<SphereHandler>().Selected(enable);
            SelectedSphere = null;
        }
            
    }

    /// <summary>
    /// enables/disables vertices of the selected wallObject
    /// </summary>
    public void EnableVertices(bool enable)
    {
        UpdateVertices();
        if(visibleVertices.Count != 0)
        {
            foreach (GameObject vertex in visibleVertices)
            {
                vertex.SetActive(enable);
            }
        }
        else if(enable)
        {
            int i = 0;
            foreach(Vector3 pos in Vertices)
            {
                GameObject obj = Instantiate(spherePrefab, transform);
                obj.GetComponent<SphereHandler>().VertexNr = i;
                obj.transform.localPosition = pos-transform.localPosition;
                obj.transform.localRotation = transform.localRotation;
                visibleVertices.Add(obj);
                i+=1;
            }
        }
    }

    /// <summary>
    /// Creates the wall when called
    /// </summary>
    public void CreateWall(GameObject parent)
    {
        
        //Can't create wall without wallData..
        if (wallData==null)
            return;

        if (Vertices.Count == 0)
            LoadData();
        PlaneCreator planeCreator = FindObjectOfType<PlaneCreator>();

        Vector3 givenPos = new Vector3(wallData.posX, wallData.posY, wallData.posZ);
        planeCreator.PlacedVertices = Vertices;
        planeCreator.CreateMesh(gameObject,givenPos);

        parent.GetComponent<Room>().AddWall(GetComponent<Wall>());
        transform.parent = parent.transform;
        transform.localEulerAngles = new Vector3(wallData.rotX, wallData.rotY, wallData.rotZ);
        transform.localPosition = givenPos;
        planeCreator.EmptyVertices();
    }

    private void UpdateVertices()
    {
        Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
        for(int i = 0; i < Vertices.Count; i++)
        {
            Vertices[i] = verts[i] + transform.localPosition;
        }
    }

    public void UpdateMesh()
    {
        //gets updated pos info
        foreach (GameObject vertex in visibleVertices)
        {
            print(vertex.GetComponent<SphereHandler>().VertexNr);
            Vertices[vertex.GetComponent<SphereHandler>().VertexNr] = vertex.transform.localPosition;
        }
        Vector3[] verts = new Vector3[]
        {
            Vertices[0],
            Vertices[1],
            Vertices[2],
            Vertices[3]
        };
        GetComponent<MeshFilter>().mesh.vertices = verts;
        //for resizeing boxCollider and centering
        Vector3 size = Vector3.zero;
        Vector3 center = Vector3.zero;
        Vector2 bigSmall = new Vector2(float.PositiveInfinity,0);
        float distance = 0;
        foreach(Vector3 pos in Vertices)
        {
            foreach(Vector3 pos2 in Vertices)
            {
                if(pos.Equals(pos2))
                    continue;
                Vector3 temp = pos - pos2;
                if(size.x < Mathf.Abs(temp.x))
                    size.x = Mathf.Abs(temp.x);
                if(size.y < Mathf.Abs(temp.y))
                    size.y = Mathf.Abs(temp.y);
                if(size.z < Mathf.Abs(temp.z))
                    size.z = Mathf.Abs(temp.z);
                float dist = Vector3.Distance(pos,pos2);
                if(dist>distance)
                {
                    distance = dist;
                    center = (pos+pos2) / 2;
                }
            }
            if(bigSmall[0] > pos.y)
                bigSmall[0] = pos.y;
            if(bigSmall[1] < pos.y)
                bigSmall[1] = pos.y;
        }
        center.y = (bigSmall[1] - bigSmall[0])/2;
        GetComponent<BoxCollider>().size = size;
        GetComponent<BoxCollider>().center = center;
    }

    public void LoadData()
    {
        foreach(Corner c in wallData.corners)
        {
            Vertices.Add(new Vector3(c.posX, c.posY, c.posZ));
        }
    }

    public void StoreData()
    {
        UpdateVertices();
        
        wallData.corners.Clear();
        Vector3 pos = transform.localPosition;
        Vector3 rot = transform.localEulerAngles;
        print(pos);
        wallData.posX = pos.x;
        wallData.posY = pos.y;
        wallData.posZ = pos.z;

        wallData.rotX = rot.x;
        wallData.rotY = rot.y;
        wallData.rotZ = rot.z;

        foreach(Vector3 vert in Vertices)
        {
            Corner corner = new Corner();
            corner.posX = vert.x;
            corner.posY = vert.y;
            corner.posZ = vert.z;

            wallData.corners.Add(corner);
        }
    }
}
public class WallData
{
    [XmlElement("PosX")]
    public float posX;
    [XmlElement("PosY")]
    public float posY;
    [XmlElement("PosZ")]
    public float posZ;

    [XmlElement("RotX")]
    public float rotX;
    [XmlElement("RotY")]
    public float rotY;
    [XmlElement("RotZ")]
    public float rotZ;

    [XmlArray("Corners")]
    [XmlArrayItem("Corner")]
    public List<Corner> corners = new List<Corner>();
}
public class Corner
{
    [XmlElement("PosX")]
    public float posX;
    [XmlElement("PosY")]
    public float posY;
    [XmlElement("PosZ")]
    public float posZ;
}
