﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.Examples.BasicCalc;
using GoogleARCore.Examples.Common;
using UnityEngine.EventSystems;

public class RoomHandler : MonoBehaviour {

    /// <summary>
    /// all the object related to ar are his child objects
    /// </summary>
    public GameObject arRelated;

    /// <summary>
    /// arController class, find ground plane and rectangles
    /// </summary>
    public mARController arController;

    public GameObject SearchingUI;

    public GameObject VirtualPlane;

    /// <summary>
    /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
    /// </summary>
    public Camera FirstPersonCamera;

    /// <summary>
    /// Camera used for room editing mode, doesn't use AR
    /// </summary>
    public EditorCameraController EditorCamera;

    /// <summary>
    /// room object that is currently edited/made
    /// </summary>
    public GameObject SelectedRoom;

    /// <summary>
    /// A model to place when a raycast from a user touch hits a plane.
    /// </summary>
    public GameObject SelectedObject;


    /// <summary>
    /// Reference to the camera that is being used
    /// </summary>
    public Camera SelectedCamera;

    /// <summary>
    /// current mode.
    /// </summary>
    private bool EditingMode = true;

    /// <summary>
    /// The toolbar in the UI, used for furniture editing
    /// </summary>
    public GameObject ToolBarUI;

    private ToolBarHandler toolbar;

    /// <summary>
    /// different tool attributes.
    /// </summary>
    public GameObject ToolAttributesUI;

    /// <summary>
    /// gameobjects related to Tool Attributes..
    /// </summary>
    private CUIColorPicker colorPicker;

    /// <summary>
    /// tools used in rotating
    /// </summary>
    public GameObject rotationTools;

    void Start()
    {
        toolbar = ToolBarUI.GetComponent<ToolBarHandler>();
        toolbar.SetSelected(ToolBarHandler.SelectedObject.None);
        toolbar.ToggleToolbar();
        colorPicker = ToolAttributesUI.GetComponentInChildren<CUIColorPicker>();
        rotationTools = ToolAttributesUI.transform.GetChild(1).gameObject;
        colorPicker.gameObject.SetActive(false);
        rotationTools.SetActive(false);
        VirtualPlane = FindObjectOfType<PlanePlacer>().gameObject;
    }


    void Update()
    {
        Touch touch;
        // If the player has not touched the screen, we are done with this update.
        if (Input.touchCount < 1)
        {
            return;
        }

        if ((touch = Input.GetTouch(0)).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = SelectedCamera.ScreenPointToRay(touch.position);
            //Ray ray = SelectedCamera.ScreenPointToRay(Input.mousePosition); //if testing on pc with mouse..
            if (Physics.Raycast(ray, out hit))
            {
                //if hitted a UI element
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    return;

                //if hitted object is already selected then end update
                if (hit.transform.gameObject == SelectedObject)
                    return;
                //Check if hitted object is furniture
                if (hit.transform.GetComponent<EditableObject>() != null)
                {
                    //other furniture is already selected, deselect it making room for other one..
                    if (SelectedObject != null)
                    {
                        //check if didn't click already selected object
                        if (SelectedObject.transform == hit.transform)
                            return;
                        Deselect();
                    }
                    SelectedObject = hit.transform.gameObject;
                    EditableObject Selected = SelectedObject.GetComponent<EditableObject>();

                    
                    //Assigns self as the controller
                    Selected.AssignController(this);
                    //Assigning needed UI elements
                    Selected.colorPicker = colorPicker;
                    Selected.rotationTools = rotationTools;
                    Selected.ToolBar = ToolBarUI;

                    if (SelectedObject.GetComponent<Furniture>() != null)
                    {
                        toolbar.SetAndToggleSelected(ToolBarHandler.SelectedObject.Furniture);
                        SelectedObject.GetComponent<Furniture>().Selected();
                    }
                    else if(SelectedObject.GetComponent<Wall>() != null)
                    {
                        toolbar.SetAndToggleSelected(ToolBarHandler.SelectedObject.Wall);
                        SelectedObject.GetComponent<Wall>().Selected();
                    }
                    else if (hit.transform.GetComponent<Room>() != null){
                        toolbar.SetAndToggleSelected(ToolBarHandler.SelectedObject.Room);
                        SelectedObject.GetComponent<Room>().Selected();
                    }
                    SelectedObject.GetComponent<EditableObject>().EnableMovementController(true);
                   
                    if (SelectedObject.GetComponent<Furniture>() != null)
                    {
                        //assigning color to the object on valueChange
                        colorPicker.SetOnValueChangeCallback(new System.Action<Color>(delegate (Color color) {
                            Selected.GetComponent<Furniture>().Model.GetComponent<Renderer>().material.color = color;
                        }));
    
                        //colorPickers color starts from SelectedFurnitures Color
                        colorPicker.Color = Selected.GetComponent<Furniture>().Model.GetComponent<Renderer>().material.color;
                    }
                    if (EditingMode)
                    {
                        EditorCamera.AllowMovement(false);
                        EditorCamera.MoveTo(SelectedObject.transform.position);
                    }
                }
                else if(hit.transform.GetComponent<ArrowHandler>() != null)
                {
                    hit.transform.GetComponent<ArrowHandler>().Move(true);
                }
                else if (SelectedObject != null)
                {
                    Deselect();
                }
            }
        }
    }

    /// <summary>
    /// Changes the mode from editing to detecting or the otherway
    /// Detecting Mode - Augmented Reality using FPS
    /// Editing Mode - Third Person Camera room editing
    /// </summary>
    public void ChangeMode()
    {
        //if editing mode, then dont use FirstPersonCamera
        if (!EditingMode)
        {
            EditorCamera.gameObject.SetActive(true);
            SelectedCamera = EditorCamera.GetComponentInChildren<Camera>();
            FirstPersonCamera.enabled = false;
            SearchingUI.SetActive(false);
            EditingMode = true;
        }
        else
        {
            arRelated.SetActive(true);
            FirstPersonCamera.enabled = true;
            SelectedCamera = FirstPersonCamera;
            EditorCamera.gameObject.SetActive(false);
            EditingMode = false;
        }
    }

    /// <summary>
    /// Returns "true" if is editing mode
    /// </summary>
    /// <returns>bool</returns>
    public bool IsEditingMode()
    {
        if (EditingMode)
            return true;
        return false;
    }

    /// <summary>
    /// Calls the Toggle Rotators method from the selected object
    /// Used by RotBtn.
    /// </summary>
    public void ToggleRotation()
    {
        if(SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().ToggleRotation();
    }

    /// <summary>
    /// Assigns the value for precision when rotating
    /// </summary>
    /// <param name="angle">Angle.</param>
    public void AssignRotatorPrecision(string angle)
    {

        float result = 0;
        if (float.TryParse(angle, out result))
        {
            SelectedObject.GetComponent<EditableObject>().AssignRotatorPrecision(result);
        }
    }

    /// <summary>
    /// Toggles precision on rotation
    /// </summary>
    public void ToggleRotatorPrecision()
    {
        if (SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().ToggleRotatorPrecision();
    }

    /// <summary>
    /// Toggles the method in rotator, that makes the object look at cameras direction
    /// </summary>
    public void LookAtCamera()
    {
        if (SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().LookAtCamera();
    }

    /// <summary>
    /// Calls the ToggleMovemenet() method for the selected object
    /// </summary>
    public void ToggleMovement()
    {
        if (SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().ToggleMovement();
    }

    /// <summary>
    /// Calls the ToggleColoring() method for the selected object
    /// </summary>
    public void ToggleColoring()
    {
        if (SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().ToggleColoring();
    }

    /// <summary>
    /// Calls the ToggleResizing() method for the selected object
    /// </summary>
    public void ToggleResizing()
    {
        if (SelectedObject != null)
            SelectedObject.GetComponent<EditableObject>().ToggleResizing();
    }

    public void ToggleEditing()
    {
        if (SelectedObject == null)
            return;
        if(SelectedObject.GetComponent<Room>() != null)
            SelectedObject.GetComponent<Room>().ToggleEditing();
        else if (SelectedObject.GetComponent<Wall>()!=null)
            SelectedObject.GetComponent<Wall>().ToggleEditing();
    }

    /// <summary>
    /// Deletes Selected object
    /// </summary>
    public void DeleteSelected()
    {
        if(SelectedObject == null)
            return;
        if (SelectedObject.GetComponent<Room>())
            GetComponent<ProjectHandler>().DeleteRoom();
        else
            SelectedRoom.GetComponent<Room>().DeleteObject(SelectedObject);
        Destroy(SelectedObject);
        SelectedObject = null;
        toolbar.SetSelected(ToolBarHandler.SelectedObject.None);
        toolbar.ToggleToolbar();
        EditorCamera.AllowMovement(true);
    }

    /// <summary>
    /// Deselects current selected object
    /// </summary>
    public void Deselect()
    {

        if (EditingMode)
        {
            EditorCamera.AllowMovement(true);
        }
        if(SelectedObject == null)
            return;
        //so it would be detected again by raycast
        if(SelectedObject.GetComponent<Wall>() != null)
             SelectedObject.GetComponent<Wall>().Deselected();   
        else if(SelectedObject.GetComponent<Furniture>() != null) 
            SelectedObject.GetComponent<Furniture>().Deselected();
        else if(SelectedObject.GetComponent<Room>() != null)
            SelectedObject.GetComponent<Room>().Deselected();
        else if(SelectedObject.GetComponent<DoorWindowObject>() != null)
            SelectedObject.GetComponent<DoorWindowObject>().Deselected();
        else
            SelectedObject.GetComponent<EditableObject>().Deselected();
        SelectedObject = null;
        toolbar.SetSelected(ToolBarHandler.SelectedObject.None);
        toolbar.ToggleToolbar();
    }

    public void NewRoomCreation(GameObject room)
    {
        SelectedRoom = room;
        ChangeMode();
    }
    public void ClearARInfo()
    {
        arRelated.GetComponentInChildren<DetectedPlaneGenerator>().m_NewPlanes.Clear();
    }
    public void RoomEditing(GameObject room)
    {
        SelectedRoom = room;
        EditorCamera.gameObject.SetActive(true);
        SelectedCamera = EditorCamera.GetComponentInChildren<Camera>();
        EditorCamera.transform.position = SelectedRoom.transform.position;
        SearchingUI.SetActive(false);
    }
    
    /// <summary>
    /// Exports selected object, if nothing selected, exports whole project
    /// </summary>
    public void ExportSelected()
    {
        bool result = false;
        //if no room or object selected, export whole project
        if(SelectedRoom == null)
        {
            GameObject project = GetComponent<ProjectHandler>().currentProject;
            ObjExporter.selectedObject = project;
            FindObjectOfType<TutorialHandler>().ChangeSuccessDescription(project.name);
            result = ObjExporter.ExportObject();
        }
        else
        {
            if(SelectedRoom != null)
            {
                ObjExporter.selectedObject = SelectedRoom;
                FindObjectOfType<TutorialHandler>().ChangeSuccessDescription(SelectedRoom.name);
            }
            else
            {
                ObjExporter.selectedObject = SelectedObject;
                FindObjectOfType<TutorialHandler>().ChangeSuccessDescription(SelectedObject.name);
            }
                
            result = ObjExporter.ExportObject();
        }
        if (result)
        {
            FindObjectOfType<TutorialHandler>().ExportSuccess();
        }
        else
        {
            FindObjectOfType<TutorialHandler>().ExportFailed();
        }
    }
}
