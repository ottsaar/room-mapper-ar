﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArrowHandler : MonoBehaviour{
    private bool selected = false;

    void Update()
    {

        if(Input.touchCount < 1)
            selected = false;
        if(!selected)
            return;
        Vector3 axis = -transform.right;
        Vector2 movement = Input.GetTouch(0).deltaPosition*Time.deltaTime/4;

        Vector3 directionVec = (Camera.main.transform.position - transform.parent.position).normalized;
        //if comperable != axis, then perpendicular, meaning gets stuck on 0 point and way is wrong.
        Vector3 comperable = transform.parent.right.Equals(-axis) ?  transform.parent.right:transform.parent.forward;
        float way =  transform.parent.right.Equals(-axis) ? -1 : 1;
        if(Vector3.SignedAngle(comperable,directionVec,Vector3.up)< 0)
            way = -way;
        transform.parent.position += new Vector3(axis.x*(way*movement.x+movement.y),axis.y*(way*movement.x+movement.y),axis.z*(way*movement.x+movement.y));
        transform.parent.GetComponent<SphereHandler>().OnChange();

    }
    
    public void Move(bool selected)
    {
        this.selected = selected;
    }
}
