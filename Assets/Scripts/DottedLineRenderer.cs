﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DottedLineRenderer : MonoBehaviour {
    public static Vector3 lastPosition;
    public static GameObject lines;
    public static Color color = new Color(1,1,1,0.75f);
    public static bool permission = false;
    public static bool doorShape = false;

    public static void DrawLine(Vector3 endPoint)
    {
        DestroyLines();
        if (!permission)
            return;
        
        if (lastPosition == null)
            return;
        lines = new GameObject();
        if (!doorShape)
        {
            DottedLine(lastPosition,endPoint);
        }
        else
        {
            //rectangle points 4 - endPoint, 1-startPoint
            // 2  4
            // 1  3  
            DottedLine(lastPosition,endPoint);
            Vector3 point2 = new Vector3(endPoint.x, lastPosition.y, endPoint.z);
            Vector3 point3 = new Vector3(lastPosition.x, endPoint.y, lastPosition.z);
            DottedLine(lastPosition,point2);//first width of rectangle
            DottedLine(point3, endPoint);//second width of rectangle
            DottedLine(lastPosition, point3);//first height of rectangle
            DottedLine(point2, endPoint);//second height of rectangle
        }
    }

    public static void DestroyLines()
    {
        if (lines == null)
            return;
        Destroy(lines);
    }

    static void DottedLine(Vector3 startPoint,  Vector3 endPoint)
    {
        float distance = Vector3.Distance(startPoint, endPoint);
        Vector3 wayToEnd = (endPoint - startPoint).normalized;
        Vector3 lastMarked = startPoint;
        ///makes lines and adds them as a child of GameObject lines, this way they can all be deleted by deleting parent obj.
        for (float i = 0f; i < distance; i += 0.2f)
        {
            Vector3 point = lastMarked + 0.1f * wayToEnd;

            float difference = distance - i;
            //so the line would end at the cursor
            if (difference < 0.1f)
            {
                point = lastMarked + difference * wayToEnd;
            }
            DrawLine(lastMarked, point, color).transform.parent = lines.transform;
            if (difference < 0.1f)
            {
                break;
            }
            lastMarked = point + 0.1f * wayToEnd;
        }
    }
    static GameObject DrawLine(Vector3 start, Vector3 end, Color color)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.0125f;
        lr.endWidth = 0.0125f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        return myLine;
    }


}
