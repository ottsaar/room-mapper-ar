﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.Examples.Common;
using GoogleARCore;

public class PlanePlacer : MonoBehaviour {
	public bool placed = false;
	private DetectedPlaneGenerator planeGen;
	void Start(){
		planeGen = FindObjectOfType<DetectedPlaneGenerator> ();
	}
	void Update(){
        if (planeGen == null)
            planeGen = FindObjectOfType<DetectedPlaneGenerator>();
        if (planeGen == null)
            return;
		//if not placed places large plane on same position in Unity world as detected plane.
		if (!placed) {
			if (planeGen.m_NewPlanes.Count > 0) {
				DetectedPlane first= planeGen.m_NewPlanes [0];
				transform.position = first.CenterPose.position;
				FindObjectOfType<RoomHandler>().SelectedRoom.transform.position = transform.position;
				placed = true;
			}
		}
	}
}
