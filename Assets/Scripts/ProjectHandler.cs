﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProjectHandler : MonoBehaviour {
    public GameObject newRoomBtn;

    public GameObject newRoom;

    public GameObject currentProject;

    public GameObject projectPrefab;
    private AppController appController;

    private RoomHandler roomHandler;

    private string newRoomName;
    
	// Use this for initialization
	void Start ()
    {
        appController = AppController.Instance;
        roomHandler = GetComponent<RoomHandler>();
        FindObjectOfType<TutorialHandler>().showTutorial = appController.tutorial;
        currentProject = Instantiate(projectPrefab, Vector3.zero,Quaternion.identity); 
        currentProject.name = appController.SelectedProject;
        
        //if selected project isn't new, then add old data back
        if (!appController.newProject)
        {
            ProjectData data = appController.selectedProjectData;
            Project project = currentProject.GetComponent<Project>();
            project.projectData = data;
            project.ClearDelegation();
            project.LoadData();
            project.VirtualPlanePos = FindObjectOfType<PlanePlacer>().transform.position;
            project.CreateRooms();
        }
        else
        {
            currentProject.GetComponent<Project>().newProject = true;
        }
        currentProject.GetComponent<Project>().OnSelectedProject();
    }

    public void SetRoomName(string name)
    {
        newRoomName = name.ToLower();
    }

    /// <summary>
    /// checks if room with given already exists
    /// </summary>
    /// <returns></returns>
    public bool checkNameAvailability()
    {   
        if(newRoomName.Replace(" ","") == "")
            return false;
        Room[] rooms = currentProject.GetComponentsInChildren<Room>();
        foreach (Room room in rooms)
        {
            if (room.name == newRoomName)
                return false;
        }
        return true;
    }

    public void EditSelectedRoom()
    {
        newRoomBtn.SetActive(false);
        roomHandler.SelectedRoom = roomHandler.SelectedObject;
        roomHandler.Deselect();
        DisableOthers();
        roomHandler.SelectedRoom.GetComponent<BoxCollider>().enabled = false;
        roomHandler.ToolBarUI.GetComponent<ToolBarHandler>().SetAndToggleSelected(ToolBarHandler.SelectedObject.None);
    }

    /// <summary>
    /// Disables all the other room in the scene
    /// </summary>
    public void DisableOthers()
    {
        if (roomHandler.SelectedRoom == null)
            return;
        currentProject.GetComponent<Project>().SelectedRoom(roomHandler.SelectedRoom.name);
    }
    
    /// <summary>
    /// Enables/Disables rooms
    /// </summary>
    /// <param name="enable">bool that shows to enable or disable rooms</param>
    public void EnableRooms(bool enable)
    {
        currentProject.GetComponent<Project>().VirtualPlanePos = FindObjectOfType<PlanePlacer>().transform.position;
        currentProject.GetComponent<Project>().ShowAllRooms();
        List<Room> rooms = currentProject.GetComponent<Project>().rooms;
    }

    public void DeleteRoom()
    {
        if (roomHandler.SelectedObject.GetComponent<Room>() != null)
            currentProject.GetComponent<Project>().RemoveRoom(roomHandler.SelectedObject.GetComponent<Room>());
    }

    /// <summary>
    /// Creates new room
    /// </summary>
    public void CreateNewRoom()
    {
        if (checkNameAvailability())
        {
            Vector3 pos = FindObjectOfType<PlanePlacer>().transform.position;
            GameObject room = Instantiate(currentProject.GetComponent<Project>().roomPrefab, new Vector3(0,pos.y,0), Quaternion.identity);
            room.transform.parent = currentProject.transform;
            room.transform.position = new Vector3(0, pos.y, 0);
            room.name = newRoomName;
            roomHandler.NewRoomCreation(room);
            currentProject.GetComponent<Project>().AddRoom(room.GetComponent<Room>());
            appController.newRoom = true;
            DisableOthers();
            newRoomBtn.SetActive(false);
            newRoom.SetActive(false);
        }
        else
        { 
            newRoom.transform.GetChild(0).gameObject.SetActive(true);
            if(newRoomName=="")
                newRoom.transform.GetChild(0).GetComponent<Text>().text = "Room name can't be empty";
            else
                newRoom.transform.GetChild(0).GetComponent<Text>().text = "Room with that name already exists";
        }
    }

    public void ToRoomMenu()
    {
        if (!roomHandler.IsEditingMode())
            roomHandler.ChangeMode();
        EnableRooms(true);
        roomHandler.Deselect();
        roomHandler.SelectedRoom = null;
        roomHandler.ClearARInfo();
        roomHandler.arRelated.SetActive(false);
        newRoomBtn.SetActive(true);
    }

    /// <summary>
    /// saves before sends user to project menu
    /// </summary>
    public void ToProjectMenu()
    {
        Save();
        appController.ToProjectMenu();
    }
    
    /// <summary>
    /// saves before send user to main menu
    /// </summary>
    public void ToMainMenu()
    {
        Save();
        appController.ToMainMenu();
    }

    public void Exit(){
        Save();
        appController.Exit();
    }
    public void Save()
    {
        //currentProject.GetComponent<Project>().StoreData();
        appController.Save();
    }
}
