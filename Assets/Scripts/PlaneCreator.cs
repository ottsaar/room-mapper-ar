﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneCreator : MonoBehaviour {
    public List<Vector3> PlacedVertices = new List<Vector3>();
    public Transform refObject;
    //walls width
    public float width = 0.25f;
    //obj to be made
    public GameObject prefab;
    private RoomHandler roomHandler;
	private float height = 0;

	// Use this for initialization
	void Start () {
        roomHandler = FindObjectOfType<RoomHandler>();
	}

    public void CreateMesh()
    {
        makeMesh(false, null, Vector3.zero);
    }

    public void CreateMesh(GameObject obj,Vector3 pos)
    {
        makeMesh(true, obj,pos);
    }

    public void CreateMesh(GameObject obj, Vector3 pos, List<Vector3> vertices)
    {
        //Storing old info
        List<Vector3> temp = PlacedVertices;
        EmptyVertices();
        //adding door/window vertices
        PlacedVertices.Add(vertices[0]);
        PlacedVertices.Add(new Vector3(vertices[0].x, vertices[1].y, vertices[0].z));
        PlacedVertices.Add(new Vector3(vertices[1].x, vertices[0].y, vertices[1].z));
        PlacedVertices.Add(vertices[1]);
        //making
        makeMesh(true, obj, pos);
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1,1);
        //Adding old info back
        obj.GetComponent<DoorWindowObject>().Vertices = PlacedVertices;
        PlacedVertices = temp;
        print("Created Mesh");
    }

    /// <summary>
    /// takes 4 last vertices from the PlacedVertices list
    /// and creates a mesh from them.
    /// </summary>
    /// <param name="givenObj"></param>
    /// <param name="obj"></param>
    /// <param name="objpos"></param>
    private void makeMesh(bool givenObj,GameObject obj,Vector3 objpos)
    {   
        int size = PlacedVertices.Count;
		List<Vector3> listOfVertices = new List<Vector3> ();
		listOfVertices.Add (PlacedVertices [size - 4]);
		listOfVertices.Add (PlacedVertices [size - 3]);
        listOfVertices.Add (PlacedVertices [size - 2]);
		listOfVertices.Add (PlacedVertices [size - 1]);
		//getting points
        Vector3 p0 = PlacedVertices[size - 4];
		Vector3 p1 = PlacedVertices[size - 2];
		//which way the normals are facing
		int[] triangles = p1.x+p1.z > p0.x+p0.z ?  new int[]{1,3,2,1,2,0, 0, 2, 1, 2, 3, 1 } : new int[]{0,2,1,2,3,1, 1, 3, 2, 1, 2, 0, };
		//calculating center
		Vector3 center = p1 - (p1 - p0) / 2;
		//calculating points as seen from center
		Vector3 p4 = PlacedVertices[size - 3] - center;
		Vector3 p5 = PlacedVertices [size - 1] - center;
		p0 = p0 - center;
		p1 = p1 - center;
        if (!givenObj)
        {
            obj = Instantiate(prefab, center, Quaternion.identity);
        }
        else
        {
            p0 = PlacedVertices[size - 4] - objpos;
            p1 = PlacedVertices[size - 2] - objpos;
            p4 = PlacedVertices[size - 3] - objpos;
            p5 = PlacedVertices[size - 1] - objpos;
        }
            

        MeshFilter filter = obj.GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mesh.name = "Sein";
        Vector3 forward;
        if (Camera.main != null)
            forward = -Camera.main.transform.forward;
        else
            forward = Vector3.forward;
		Vector3[] normals = new Vector3[8] {
			forward,
            forward,
            forward,
            forward,
            -forward,
            -forward,
            -forward,
            -forward
        };

        Vector3[] vertices = new Vector3[8]
        {
	        // Front
			p0,p4,p1,p5,
            //back
			p0,p4,p1,p5
        };
        Vector2 _00 = new Vector2(0f, 0f);
        Vector2 _10 = new Vector2(1f, 0f);
        Vector2 _01 = new Vector2(0f, 1f);
        Vector2 _11 = new Vector2(1f, 1f);

        Vector2[] uvs = new Vector2[]
        {
	        // Front
			_00, _10, _01,  _11,
            //back
            _11,_01,_10,_00
        };
		mesh.vertices = vertices;
        mesh.uv = uvs;
		mesh.triangles = triangles;
		mesh.normals = normals;
        filter.mesh = mesh;
        if(obj.GetComponent<Wall>() != null)
		    obj.GetComponent<Wall> ().Vertices = listOfVertices;

        //Calculating tiling of the texture
        float xz = Mathf.Round(2 * Mathf.Sqrt(Mathf.Pow(Mathf.Abs (p1.x - p0.x),2)+Mathf.Pow(Mathf.Abs (p1.z - p0.z),2)));
		float y = Mathf.Round(2 * Mathf.Abs (p0.y - p4.y));

		xz = xz==0 ? 1 : xz; //if result is 0 then make to 1
		y = y == 0 ? 1 : y; //if result is 0 then make to 1
        print("tiling:" + xz + ";" + y);
		//applying tiling
		obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(y,xz);

        //if obj is not given then we must add a parent to it
        if(!givenObj)
        {
            roomHandler.SelectedRoom.GetComponent<Room>().AddWall(obj.GetComponent<Wall>());
            obj.transform.parent = roomHandler.SelectedRoom.transform;
        }

        if(obj.GetComponent<MeshCollider>() == null){
            obj.AddComponent<MeshCollider>();
        }
          
    }

	/// <summary>
    /// Handles the vertec placing
    /// after certain amount(4) makes a wall
    /// </summary>
    public void PlaceVertex()
    {
        //adding vertices
        PlacedVertices.Add(refObject.position);
		if (height == 0) {
			if (PlacedVertices.Count >= 2) {
				Vector3 lastVert = PlacedVertices [PlacedVertices.Count - 2];
				//if x and z are equal then assign new height.
				if (!ObjToRay.putOn) {
					height = refObject.position.y - lastVert.y;
				}
			}
            ObjToRay.DoPut();
		}
		// if height has been assigned, make another vertex out of placed vertex
		else
		{
			PlacedVertices.Add(refObject.position + new Vector3 (0, height, 0));
		}
        //creating mesh
        if(PlacedVertices.Count - 4 >= 0 && ((PlacedVertices.Count-4)%2 == 0 || PlacedVertices.Count-4 == 0))
        {
            CreateMesh();
        }
    }

	/// <summary>
    /// zeros the height variable
    /// </summary>
	public void zeroHeight(){
		height = 0;
	}
	public void EmptyVertices(){
		PlacedVertices = new List<Vector3> ();
	}
    
    public void Reset()
    {
        zeroHeight();
        EmptyVertices();
    }
}
