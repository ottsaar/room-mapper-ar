﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereHandler : MonoBehaviour {
    public int VertexNr=0;
    List<GameObject> axes = new List<GameObject>();
    void Start()
    {
        axes.Add(transform.GetChild(0).gameObject);
        axes.Add(transform.GetChild(1).gameObject);
        axes.Add(transform.GetChild(2).gameObject);
    }

    public void Selected(bool enable)
    {
        foreach(GameObject go in axes)
        {
            go.SetActive(enable);
        }
    }

    public void OnChange()
    {
        transform.parent.GetComponent<Wall>().UpdateMesh();
    }
}
