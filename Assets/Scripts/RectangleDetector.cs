﻿namespace GoogleARCore.Examples.ComputerVision
{
	using System.Collections;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using GoogleARCore;
	using UnityEngine;
	using UnityEngine.UI;


	#if UNITY_EDITOR
	// Set up touch input propagation while using Instant Preview in the editor.
	using Input = InstantPreviewInput;
	#endif  // UNITY_EDITOR

	public class RectangleDetector : MonoBehaviour {
        public float refreshRate = 2;
		public ARCoreSession ARSessionManager;

		private PlaneCreator planeCreator;
		private LineDetector detector;
        private float timer;

		/// <summary>
		/// A buffer that stores the result of performing edge detection on the camera image each frame.
		/// </summary>
		private byte[] m_EdgeDetectionResultImage = null;
		private Dictionary<Vector2,int> lines;
		private List<Vector2[]> fourLines;
		private CameraImageBytes image;
		private bool detected=false;
		private RectanglePlacer rectanglePlacer;

		void Start()
        {
			planeCreator = FindObjectOfType<PlaneCreator> ();
			rectanglePlacer = FindObjectOfType<RectanglePlacer> ();
            timer = Time.time;
		}
		void Update()
        {
			if (!Session.Status.IsValid())
			{
				return;
			}

            if (timer + 2 <= Time.time)
            {
                using (var image = Frame.CameraImage.AcquireCameraImageBytes())
                {
                    if (!image.IsAvailable)
                    {
                        m_EdgeDetectionResultImage = null;
                        return;
                    }
                    this.image = image;
                    _OnImageAvailable(image.Width, image.Height, image.YRowStride, image.Y, 0);
                }
                
                lines = LineDetector.DetectLines(m_EdgeDetectionResultImage, image.Width, image.Height,Screen.currentResolution.width,Screen.currentResolution.height);
                Debug.Log("LINES:" + lines.Count);
                //At this point, we have too many lines and lag might be too high
                if(lines.Count > 15000)
                {
                    LineDetector.pixelStep += 1;
                    return;
                }
                else
                    LineDetector.pixelStep = 6;

                fourLines = LineDetector.FilteringOutLines(lines);
                if(fourLines.Count == 0 || fourLines.Count < 50)
                {
                    if(LineDetector.threshold > 6)
                        LineDetector.threshold -= 1;
                }
                if(fourLines.Count > 2000)
                {
                    LineDetector.threshold += 1;
                    return;
                }

                List<Vector2[]> biggest = LineDetector.fiveBiggestRectangles(fourLines);

                timer = Time.time;

                if (biggest.Count == 0)
                    return;
                Debug.Log("Biggest count:" + biggest.Count);
                rectanglePlacer.ShowingCorners(biggest);
            }
		}

        /// Handles a new CPU image.
        /// </summary>
        /// <param name="width">Width of the image, in pixels.</param>
        /// <param name="height">Height of the image, in pixels.</param>
        /// <param name="rowStride">Row stride of the image, in pixels.</param>
        /// <param name="pixelBuffer">Pointer to raw image buffer.</param>
        /// <param name="bufferSize">The size of the image buffer, in bytes.</param>
        private void _OnImageAvailable(int width, int height, int rowStride, IntPtr pixelBuffer, int bufferSize)
		{
			m_EdgeDetectionResultImage = new byte[width * height];
            // Detect edges within the image.
            detected = EdgeDetector.Detect(m_EdgeDetectionResultImage, pixelBuffer, width, height, rowStride);
			
		}
    }
}
